package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;

@RunWith(value = JUnitParamsRunner.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Test
    @TestCaseName("Run testGetByLogin for getByLogin({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersCaseData"
    )
    public void testGetByLogin(
            @NotNull final User user
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(this.userRepository);
        @NotNull final User addRecord = this.userRepository.addRecord(user);
        Assert.assertNotNull(addRecord);

        @Nullable final User getUser = this.userRepository.getByLogin(addRecord.getLogin());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(user.getId(), getUser.getId());
        Assert.assertEquals(user.getLogin(), getUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), getUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), getUser.getRole());
    }

    @Test(expected = EmptyUserException.class)
    @TestCaseName("Run testNegativeDeleteByLogin for deleteByLogin({0})")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersCaseData"
    )

    public void testNegativeDeleteByLogin(
            @NotNull final User user
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(this.userRepository);
        this.userRepository.deleteByLogin(user.getLogin());
    }

    @Test
    @TestCaseName("Run testDeleteByLogin for deleteByLogin({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersCaseData"
    )
    public void testDeleteByLogin(
            @NotNull final User user
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(this.userRepository);
        @NotNull final User addRecord = this.userRepository.addRecord(user);
        Assert.assertNotNull(addRecord);

        @Nullable final User deleteUser = this.userRepository.deleteByLogin(user.getLogin());
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(user.getId(), deleteUser.getId());
        Assert.assertEquals(user.getLogin(), deleteUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), deleteUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), deleteUser.getRole());
    }

}