package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDomainProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.util.FileUtil;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class AdminDataInterChangeEndpointTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IAuthenticationService authService = serviceLocator.getAuthenticationService();

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final ITaskUserService taskService = serviceLocator.getTaskService();

    @NotNull
    private final IProjectUserService projectService = serviceLocator.getProjectService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final IAdminDataInterChangeEndpoint adminDataInterChangeEndpoint = new AdminDataInterChangeEndpoint(serviceLocator);

    @Before
    public void initialAdminRecordBefore() {
        @NotNull final User addAdminRecord = this.userService.addUser(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addAdminRecord);
    }

    @Before
    public void createTempFilesBefore() {
        this.configService.load();
        FileUtil.create(this.configService.getBase64FileName());
        FileUtil.create(this.configService.getBinFileName());
        FileUtil.create(this.configService.getBase64FileName());
        FileUtil.create(this.configService.getJsonFileName());
        FileUtil.create(this.configService.getXmlFileName());
        FileUtil.create(this.configService.getYamlFileName());
    }

    @After
    public void clearTempFilesAfter() {
        FileUtil.delete(this.configService.getBase64FileName());
        FileUtil.delete(this.configService.getBinFileName());
        FileUtil.delete(this.configService.getBase64FileName());
        FileUtil.delete(this.configService.getJsonFileName());
        FileUtil.delete(this.configService.getXmlFileName());
        FileUtil.delete(this.configService.getYamlFileName());
    }

    @Test
    @TestCaseName("Run testDataBinClear for dataBinClear(session)")
    public void testDataBinClear() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        Assert.assertNotNull(this.sessionService);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isBinClear = this.adminDataInterChangeEndpoint.dataBinClear(open);
        Assert.assertTrue(isBinClear);
    }

    @Test
    @TestCaseName("Run testDataBase64Clear for dataBase64Clear(session)")
    public void testDataBase64Clear() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        Assert.assertNotNull(this.sessionService);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isBase64Clear = this.adminDataInterChangeEndpoint.dataBase64Clear(open);
        Assert.assertTrue(isBase64Clear);
    }

    @Test
    @TestCaseName("Run testDataJsonClear for dataJsonClear(session)")
    public void testDataJsonClear() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        Assert.assertNotNull(this.sessionService);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isJsonClear = this.adminDataInterChangeEndpoint.dataJsonClear(open);
        Assert.assertTrue(isJsonClear);
    }

    @Test
    @TestCaseName("Run testDataXmlClear for dataXmlClear(session)")
    public void testDataXmlClear() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        Assert.assertNotNull(this.sessionService);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isXmlClear = this.adminDataInterChangeEndpoint.dataXmlClear(open);
        Assert.assertTrue(isXmlClear);
    }

    @Test
    @TestCaseName("Run testDataYamlClear for dataYamlClear(session)")
    public void testDataYamlClear() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        Assert.assertNotNull(this.sessionService);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isYamlClear = this.adminDataInterChangeEndpoint.dataYamlClear(open);
        Assert.assertTrue(isYamlClear);
    }

    @Test
    @TestCaseName("Run testExportDataBin for exportDataBin(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataBin(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataBin(open);
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataBase64 for exportDataBase64(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataBase64(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataBase64(open);
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataJson for exportDataJson(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataJson(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataJson(open);
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataXml for exportDataXml(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataXml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataXml(open);
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataYaml for exportDataYaml(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataYaml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataYaml(open);
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testImportDataBin for importDataBin(open)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataBin(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataBin(open);
        Assert.assertNotNull(domain);

        @NotNull final Domain importDataBin = this.adminDataInterChangeEndpoint.importDataBin(open);
        Assert.assertNotNull(importDataBin);
        Assert.assertEquals(this.userService.getAllRecords(), importDataBin.getUsers());
        Assert.assertEquals(this.taskService.getAllRecords(), importDataBin.getTasks());
        Assert.assertEquals(this.projectService.getAllRecords(), importDataBin.getProjects());
    }

    @Test
    @TestCaseName("Run testImportDataBase64 for importDataBase64(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataBase64(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataBase64(open);
        Assert.assertNotNull(domain);

        @NotNull final Domain importDataBase64 = this.adminDataInterChangeEndpoint.importDataBase64(open);
        Assert.assertNotNull(importDataBase64);
        Assert.assertEquals(this.userService.getAllRecords(), importDataBase64.getUsers());
        Assert.assertEquals(this.taskService.getAllRecords(), importDataBase64.getTasks());
        Assert.assertEquals(this.projectService.getAllRecords(), importDataBase64.getProjects());
    }

    @Test
    @TestCaseName("Run testImportDataJson for importDataJson(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataJson(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataJson(open);
        Assert.assertNotNull(domain);

        @NotNull final Domain importDataJson = this.adminDataInterChangeEndpoint.importDataJson(open);
        Assert.assertNotNull(importDataJson);
        Assert.assertEquals(this.userService.getAllRecords(), importDataJson.getUsers());
        Assert.assertEquals(this.taskService.getAllRecords(), importDataJson.getTasks());
        Assert.assertEquals(this.projectService.getAllRecords(), importDataJson.getProjects());
    }

    @Test
    @TestCaseName("Run testImportDataXml for importDataXml(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataXml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataXml(open);
        Assert.assertNotNull(domain);

        @NotNull final Domain importDataXml = this.adminDataInterChangeEndpoint.importDataXml(open);
        Assert.assertNotNull(importDataXml);
        Assert.assertEquals(this.userService.getAllRecords(), importDataXml.getUsers());
        Assert.assertEquals(this.taskService.getAllRecords(), importDataXml.getTasks());
        Assert.assertEquals(this.projectService.getAllRecords(), importDataXml.getProjects());
    }

    @Test
    @TestCaseName("Run testImportDataYaml for importDataYaml(session)")
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataYaml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final User addRecord = this.authService.signUp(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addRecord);
        setAllUserRecords.add(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Domain domain = this.adminDataInterChangeEndpoint.exportDataYaml(open);
        Assert.assertNotNull(domain);

        @NotNull final Domain importDataYaml = this.adminDataInterChangeEndpoint.importDataYaml(open);
        Assert.assertNotNull(importDataYaml);
        Assert.assertEquals(this.userService.getAllRecords(), importDataYaml.getUsers());
        Assert.assertEquals(this.taskService.getAllRecords(), importDataYaml.getTasks());
        Assert.assertEquals(this.projectService.getAllRecords(), importDataYaml.getProjects());
    }

}