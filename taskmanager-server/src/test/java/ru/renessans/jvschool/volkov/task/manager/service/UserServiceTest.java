package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.*;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Test(expected = EmptyLoginException.class)
    @TestCaseName("Run testNegativeGetUserByLogin for getUserByLogin(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetUserByLogin(
            @Nullable final String login
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        this.userService.getUserByLogin(login);
    }

    @Test
    @TestCaseName("Run testNegativeAddUser for addUser(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersAllFieldsCaseData"
    )
    public void testNegativeAddUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName,
            @Nullable final UserRole userRole
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);

        @NotNull final EmptyLoginException loginThrown = assertThrows(
                EmptyLoginException.class,
                () -> this.userService.addUser(login, password)
        );
        Assert.assertNotNull(loginThrown);
        Assert.assertNotNull(loginThrown.getMessage());

        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
        @NotNull final EmptyPasswordException passwordThrown = assertThrows(
                EmptyPasswordException.class,
                () -> this.userService.addUser(tempLogin, password)
        );
        Assert.assertNotNull(passwordThrown);
        Assert.assertNotNull(passwordThrown.getMessage());

        @NotNull final String tempPassword = DemoDataConst.USER_DEFAULT_PASSWORD;
        @NotNull final EmptyFirstNameException firstNameThrown = assertThrows(
                EmptyFirstNameException.class,
                () -> this.userService.addUser(tempLogin, tempPassword, firstName)
        );
        Assert.assertNotNull(firstNameThrown);
        Assert.assertNotNull(firstNameThrown.getMessage());

        @NotNull final EmptyUserRoleException roleThrown = assertThrows(
                EmptyUserRoleException.class,
                () -> this.userService.addUser(tempLogin, tempPassword, userRole)
        );
        Assert.assertNotNull(roleThrown);
        Assert.assertNotNull(roleThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdatePasswordById for updatePasswordById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeUpdatePasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);

        @NotNull final EmptyUserIdException idThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.userService.updatePasswordById(id, newPassword)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());

        @NotNull final String tempId = UUID.randomUUID().toString();
        Assert.assertNotNull(tempId);
        @NotNull final EmptyPasswordException newPasswordThrown = assertThrows(
                EmptyPasswordException.class,
                () -> this.userService.updatePasswordById(tempId, newPassword)
        );
        Assert.assertNotNull(newPasswordThrown);
        Assert.assertNotNull(newPasswordThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeEditProfileById for editProfileById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeEditProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);

        @NotNull final EmptyUserIdException idThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.userService.editProfileById(id, firstName)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());

        @NotNull final String tempFileName = DemoDataConst.USER_DEFAULT_FIRSTNAME;
        Assert.assertNotNull(tempFileName);
        @NotNull final EmptyFirstNameException fileNameThrown = assertThrows(
                EmptyFirstNameException.class,
                () -> this.userService.editProfileById(tempFileName, firstName)
        );
        Assert.assertNotNull(fileNameThrown);
        Assert.assertNotNull(fileNameThrown.getMessage());
    }

    @Test(expected = EmptyLoginException.class)
    @TestCaseName("Run testNegativeLockUserByLogin for lockUserByLogin(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeLockUserByLogin(
            @Nullable final String login
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        this.userService.lockUserByLogin(login);
    }

    @Test(expected = EmptyLoginException.class)
    @TestCaseName("Run testNegativeUnlockUserByLogin for unlockUserByLogin(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeUnlockUserByLogin(
            @Nullable final String login
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        this.userService.unlockUserByLogin(login);
    }

    @Test(expected = EmptyLoginException.class)
    @TestCaseName("Run testNegativeDeleteUserByLogin for deleteUserByLogin(\"{0}\")")
    @Category(NegativeImplementation.class)
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeDeleteUserByLogin(
            @Nullable final String login
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        this.userService.deleteUserByLogin(login);
    }

    @Test
    @TestCaseName("Run testGetUserByLogin for getUserByLogin(\"{0}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testGetUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);

        @Nullable final User getUser = this.userService.getUserByLogin(login);
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
        Assert.assertEquals(hashPassword, getUser.getPasswordHash());
    }

    @Test
    @TestCaseName("Run testAddUser for addUser(\"{0}\", \"{1}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testAddUser(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);

        @NotNull final User addUser = this.userService.addUser(login, password);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
    }

    @Test
    @TestCaseName("Run testAddUserWithFirstName for addUser(\"{0}\", \"{1}\", \"{2}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithNewEntityCaseData"
    )
    public void testAddUserWithFirstName(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(firstName);

        @NotNull final User addUser = this.userService.addUser(login, password, firstName);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(firstName, addUser.getFirstName());
    }

    @Test
    @TestCaseName("Run testAddUserWithRole for addUser(\"{0}\", \"{1}\", {2})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithRoleCaseData"
    )
    public void testAddUserWithRole(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final UserRole userRole
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(userRole);

        @NotNull final User addUser = this.userService.addUser(login, password, userRole);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(userRole, addUser.getRole());
    }

    @Test
    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(id, \"{2}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithNewEntityCaseData"
    )
    public void testUpdatePasswordById(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String newPassword
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(newPassword);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);

        @Nullable final User updatePassword = this.userService.updatePasswordById(addRecord.getId(), newPassword);
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
        Assert.assertEquals(login, updatePassword.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(newPassword);
        Assert.assertEquals(addRecord.getRole(), updatePassword.getRole());
        Assert.assertEquals(hashPassword, updatePassword.getPasswordHash());
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfileById(id, \"{2}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithNewEntityCaseData"
    )
    public void testEditProfileById(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String newFirstName
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(newFirstName);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);

        @Nullable final User editUser = this.userService.editProfileById(addRecord.getId(), newFirstName);
        Assert.assertNotNull(editUser);
        Assert.assertEquals(addRecord.getId(), editUser.getId());
        Assert.assertEquals(login, editUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, editUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), editUser.getRole());
        Assert.assertEquals(newFirstName, editUser.getFirstName());
    }

    @Test
    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(\"{0}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testLockUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);

        @Nullable final User lockUser = this.userService.lockUserByLogin(login);
        Assert.assertNotNull(lockUser);
        Assert.assertEquals(addRecord.getId(), lockUser.getId());
        Assert.assertEquals(login, lockUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, lockUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), lockUser.getRole());
        Assert.assertTrue(lockUser.getLockdown());
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(\"{0}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testUnlockUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @Nullable final User lockUser = this.userService.lockUserByLogin(login);
        Assert.assertNotNull(lockUser);

        @Nullable final User unlockUser = this.userService.unlockUserByLogin(login);
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
        Assert.assertEquals(login, unlockUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, unlockUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), unlockUser.getRole());
        Assert.assertFalse(unlockUser.getLockdown());
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for lockUserByLogin(\"{0}\")")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testDeleteUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);

        @Nullable final User deleteUser = this.userService.deleteUserByLogin(login);
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, deleteUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
    }

    @Test
    @TestCaseName("Run testInitialDemoData for initialDemoData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testInitialDemoData() {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        @NotNull final Collection<User> initialUsers = this.userService.initialDemoData();
        Assert.assertNotNull(initialUsers);
        Assert.assertNotEquals(0, initialUsers.size());
    }

}