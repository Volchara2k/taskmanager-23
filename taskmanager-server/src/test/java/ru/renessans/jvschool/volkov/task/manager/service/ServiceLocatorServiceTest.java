package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;

@RunWith(value = JUnitParamsRunner.class)
public final class ServiceLocatorServiceTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocatorService = new ServiceLocatorService(serviceLocatorRepository);

    @Test
    @TestCaseName("Run testGetUserService for getUserService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetUserService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final IUserService userService = this.serviceLocatorService.getUserService();
        Assert.assertNotNull(userService);
    }

    @Test
    @TestCaseName("Run testGetSessionService for getSessionService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final ISessionService sessionService = this.serviceLocatorService.getSessionService();
        Assert.assertNotNull(sessionService);
    }

    @Test
    @TestCaseName("Run testGetAuthenticationService for getAuthenticationService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetAuthenticationService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final IAuthenticationService authService = this.serviceLocatorService.getAuthenticationService();
        Assert.assertNotNull(authService);
    }

    @Test
    @TestCaseName("Run testGetTaskService for getTaskService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetTaskService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final ITaskUserService taskService = this.serviceLocatorService.getTaskService();
        Assert.assertNotNull(taskService);
    }

    @Test
    @TestCaseName("Run testGetProjectService for getProjectService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetProjectService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final IProjectUserService projectService = this.serviceLocatorService.getProjectService();
        Assert.assertNotNull(projectService);
    }

    @Test
    @TestCaseName("Run testGetDataInterChangeService for getDataInterChangeService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetDataInterChangeService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final IDataInterChangeService dataService = this.serviceLocatorService.getDataInterChangeService();
        Assert.assertNotNull(dataService);
    }

    @Test
    @TestCaseName("Run testGetConfigurationService for getConfigurationService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetConfigurationService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocatorService);
        @NotNull final IConfigurationService configService = this.serviceLocatorService.getConfigurationService();
        Assert.assertNotNull(configService);
    }

}