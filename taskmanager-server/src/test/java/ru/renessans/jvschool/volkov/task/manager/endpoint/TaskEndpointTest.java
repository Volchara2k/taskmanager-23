package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ITaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class TaskEndpointTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final ITaskUserService taskService = serviceLocator.getTaskService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @Before
    public void loadConfigurationBefore() {
        Assert.assertNotNull(this.configService);
        this.configService.load();
    }

    @Test
    @TestCaseName("Run testAddTask for addTask(session, {0}, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testAddTask(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @NotNull final Task addTask = this.taskEndpoint.addTask(
                open, task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTask);
        Assert.assertEquals(addRecord.getId(), addTask.getUserId());
        Assert.assertEquals(task.getTitle(), addTask.getTitle());
        Assert.assertEquals(task.getDescription(), addTask.getDescription());
    }

    @Test
    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testUpdateTaskByIndex(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);

        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        task.setTitle(newDescription);
        @Nullable final Task updateTask =
                this.taskEndpoint.updateTaskByIndex(open, 0, task.getTitle(), newDescription);
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(addTaskRecord.getId(), updateTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), updateTask.getUserId());
        Assert.assertEquals(task.getTitle(), updateTask.getTitle());
    }

    @Test
    @TestCaseName("Run testUpdateTaskById for updateTaskById({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testUpdateTaskById(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        task.setTitle(newDescription);
        @Nullable final Task updateTask = this.taskEndpoint.updateTaskById(
                open, addTaskRecord.getId(), task.getTitle(), newDescription
        );
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(addTaskRecord.getId(), updateTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), updateTask.getUserId());
        Assert.assertEquals(task.getTitle(), updateTask.getTitle());
    }

    @Test
    @TestCaseName("Run testDeleteTaskById for deleteTaskById({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteTaskById(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Task deleteTask = this.taskEndpoint.deleteTaskById(open, addTaskRecord.getId());
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(addTaskRecord.getId(), deleteTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(task.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(task.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteTaskByIndex(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Task deleteTask = this.taskEndpoint.deleteTaskByIndex(open, 0);
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(addTaskRecord.getId(), deleteTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(task.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(task.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteTaskByTitle(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Task deleteTask = this.taskEndpoint.deleteTaskByTitle(open, addTaskRecord.getTitle());
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(addTaskRecord.getId(), deleteTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(task.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(task.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteAllTasks(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<Task> deleteTasks = this.taskEndpoint.deleteAllTasks(open);
        Assert.assertNotNull(deleteTasks);
        Assert.assertNotEquals(0, deleteTasks.size());
        final boolean isUserTasks = deleteTasks.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetTaskById for getTaskById({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetTaskById(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Task getTask = this.taskEndpoint.getTaskById(open, addTaskRecord.getId());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTaskRecord.getId(), getTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), getTask.getUserId());
        Assert.assertEquals(task.getTitle(), getTask.getTitle());
        Assert.assertEquals(task.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetTaskByIndex(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Task getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTaskRecord.getId(), getTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), getTask.getUserId());
        Assert.assertEquals(task.getTitle(), getTask.getTitle());
        Assert.assertEquals(task.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetTaskByTitle(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Task getTask = this.taskEndpoint.getTaskByTitle(open, addTaskRecord.getTitle());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTaskRecord.getId(), getTask.getId());
        Assert.assertEquals(addTaskRecord.getUserId(), getTask.getUserId());
        Assert.assertEquals(task.getTitle(), getTask.getTitle());
        Assert.assertEquals(task.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetAllTasks for getAllTasks({0}, 0, {1})")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetAllTasks(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.taskEndpoint);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Task addTaskRecord = this.taskService.add(
                addRecord.getId(), task.getTitle(), task.getDescription()
        );
        Assert.assertNotNull(addTaskRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<Task> getAllTasks = this.taskEndpoint.getAllTasks(open);
        Assert.assertNotNull(getAllTasks);
        Assert.assertNotEquals(0, getAllTasks.size());
        final boolean isUserTasks = getAllTasks.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

}