package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepository;

import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class ProjectUserServiceTest {

    @NotNull
    private final IProjectUserRepository projectUserRepository = new ProjectUserRepository();

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(projectUserRepository);

    @Test
    @TestCaseName("Run testNegativeAdd for add(\"{0}\", \"{1}\", \"{2}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerMainFieldsCaseData"
    )
    public void testNegativeAdd(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.add(userId, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final EmptyTitleException titleThrown = assertThrows(
                EmptyTitleException.class,
                () -> this.projectService.add(userIdTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = DemoDataConst.TASK_TITLE;
        @NotNull final EmptyDescriptionException descriptionThrown = assertThrows(
                EmptyDescriptionException.class,
                () -> this.projectService.add(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdateByIndex for updateByIndex(\"{0}\", {1}, \"{2}\", \"{3}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerWithIndexCaseData"
    )
    public void testNegativeUpdateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.updateByIndex(userId, index, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> this.projectService.updateByIndex(userIdTemp, index, title, description)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());

        @NotNull final Integer indexTemp = 0;
        Assert.assertNotNull(indexTemp);
        @NotNull final EmptyTitleException titleThrown = assertThrows(
                EmptyTitleException.class,
                () -> this.projectService.updateByIndex(userIdTemp, indexTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = DemoDataConst.TASK_TITLE;
        @NotNull final EmptyDescriptionException descriptionThrown = assertThrows(
                EmptyDescriptionException.class,
                () -> this.projectService.add(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdateById for updateById(\"{0}\", \"{1}\", \"{2}\", \"{3}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerWithIdCaseData"
    )
    public void testNegativeUpdateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.updateById(userId, id, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final EmptyIdException indexThrown = assertThrows(
                EmptyIdException.class,
                () -> this.projectService.updateById(userIdTemp, id, title, description)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());

        @NotNull final String idTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(idTemp);
        @NotNull final EmptyTitleException titleThrown = assertThrows(
                EmptyTitleException.class,
                () -> this.projectService.updateById(userIdTemp, idTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = DemoDataConst.TASK_TITLE;
        @NotNull final EmptyDescriptionException descriptionThrown = assertThrows(
                EmptyDescriptionException.class,
                () -> this.projectService.add(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteByIndex for deleteByIndex(\"{0}\", {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerIndexCaseData"
    )
    public void testNegativeDeleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.deleteByIndex(userId, index)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> this.projectService.deleteByIndex(userIdTemp, index)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteById for deleteById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeDeleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.deleteById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final EmptyIdException idThrown = assertThrows(
                EmptyIdException.class,
                () -> this.projectService.deleteById(userIdTemp, id)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeDeleteByTitle(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.deleteById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final EmptyTitleException titleThrown = assertThrows(
                EmptyTitleException.class,
                () -> this.projectService.deleteByTitle(userIdTemp, id)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());
    }

    @Test(expected = EmptyUserIdException.class)
    @TestCaseName("Run testNegativeDeleteAll for deleteAll(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeDeleteAll(
            @Nullable final String userId
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        this.projectService.deleteAll(userId);
    }

    @Test
    @TestCaseName("Run testNegativeGetByIndex for getByIndex(\"{0}\", {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerIndexCaseData"
    )
    public void testNegativeGetByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.getByIndex(userId, index)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> this.projectService.getByIndex(userIdTemp, index)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetById for getById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeGetById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.getById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final EmptyIdException idThrown = assertThrows(
                EmptyIdException.class,
                () -> this.projectService.getById(userIdTemp, id)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetByTitle for getByTitle(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeGetByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);

        @NotNull final EmptyUserIdException userIdThrown = assertThrows(
                EmptyUserIdException.class,
                () -> this.projectService.getByTitle(userId, title)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final EmptyTitleException titleThrown = assertThrows(
                EmptyTitleException.class,
                () -> this.projectService.getByTitle(userIdTemp, title)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());
    }

    @Test(expected = EmptyUserIdException.class)
    @TestCaseName("Run testNegativeGetAll for getAll(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetAll(
            @Nullable final String userId
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        this.projectService.getAll(userId);
    }

    @Test(expected = EmptyUserException.class)
    @TestCaseName("Run testInitialDemoData for initialDemoData({0})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidCollectionsUsersCaseData"
    )
    public void testNegativeInitialDemoData(
            @Nullable final Collection<User> users
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        this.projectService.initialDemoData(users);
    }

    @Test
    @TestCaseName("Run testAdd for add({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testAdd(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);

        @NotNull final Project addProject = this.projectService.add(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProject);
        Assert.assertEquals(project.getUserId(), addProject.getUserId());
        Assert.assertEquals(project.getTitle(), addProject.getTitle());
        Assert.assertEquals(project.getDescription(), addProject.getDescription());
    }

    @Test
    @TestCaseName("Run testUpdateByIndex for updateByIndex({0}, 0, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testUpdateByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        project.setTitle(newDescription);
        @Nullable final Project updateProject =
                this.projectService.updateByIndex(user.getId(), 0, project.getTitle(), newDescription);
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(project.getId(), updateProject.getId());
        Assert.assertEquals(project.getUserId(), updateProject.getUserId());
        Assert.assertEquals(project.getTitle(), updateProject.getTitle());
    }

    @Test
    @TestCaseName("Run testUpdateById for updateById({0}, {1}")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testUpdateById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @NotNull final String newTitle = UUID.randomUUID().toString();
        Assert.assertNotNull(newTitle);
        project.setTitle(newTitle);
        @Nullable final Project updateProject =
                this.projectService.updateById(user.getId(), project.getId(), newTitle, project.getDescription());
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(project.getId(), updateProject.getId());
        Assert.assertEquals(project.getUserId(), updateProject.getUserId());
        Assert.assertEquals(project.getDescription(), updateProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteByIndex for deleteByIndex({0}, 0)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project deleteProject = this.projectService.deleteByIndex(user.getId(), 0);
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(project.getId(), deleteProject.getId());
        Assert.assertEquals(project.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteById for deleteById({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project deleteProject = this.projectService.deleteById(user.getId(), project.getId());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(project.getId(), deleteProject.getId());
        Assert.assertEquals(project.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteByTitle for deleteByTitle({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project deleteProject = this.projectService.deleteByTitle(user.getId(), project.getTitle());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(project.getId(), deleteProject.getId());
        Assert.assertEquals(project.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteAll for deleteAll({0})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteAll(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Collection<Project> deleteProjects = this.projectService.deleteAll(user.getId());
        Assert.assertNotNull(deleteProjects);
        Assert.assertNotEquals(0, deleteProjects.size());
        final boolean isUserTasks = deleteProjects.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetByIndex for getByIndex({0}, 0)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project getProject = this.projectService.getByIndex(user.getId(), 0);
        Assert.assertNotNull(getProject);
        Assert.assertEquals(project.getId(), getProject.getId());
        Assert.assertEquals(project.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testGetById for getById({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project getProject = this.projectService.getById(user.getId(), project.getId());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(project.getId(), getProject.getId());
        Assert.assertEquals(project.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testGetByTitle for getByTitle({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project getProject = this.projectService.getByTitle(user.getId(), project.getTitle());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(project.getId(), getProject.getId());
        Assert.assertEquals(project.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testGetAll for getAll({0})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetAll(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectUserRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Collection<Project> getProjects = this.projectService.getAll(user.getId());
        Assert.assertNotNull(getProjects);
        Assert.assertNotEquals(0, getProjects.size());
        final boolean isUserTasks = getProjects.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testInitialDemoData for initialDemoData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validCollectionUsersCaseData"
    )
    public void testInitialDemoData(
            @NotNull final Collection<User> users
    ) {
        Assert.assertNotNull(this.projectUserRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(users);
        @NotNull final Collection<Project> initProjects = this.projectService.initialDemoData(users);
        Assert.assertNotNull(initProjects);
        Assert.assertNotEquals(0, initProjects.size());
    }

}