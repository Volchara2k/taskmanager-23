package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataInterChangeProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.file.EmptyFilenameException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyKeyException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import java.io.File;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
public final class DataMarshalizerUtilTest {

    @Rule
    @NotNull
    public TemporaryFolder file = new TemporaryFolder();

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeWriteToJsonWithoutObject for writeToJson(null, random)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeWriteToJsonWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataMarshalizerUtil.writeToJson(null, filename);
    }

    @Test(expected = EmptyFilenameException.class)
    @TestCaseName("Run testNegativeWriteToJsonWithoutFilename for writeToJson({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesCaseData"
    )
    public void testNegativeWriteToJsonWithoutFilename(
            @NotNull final Object object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataMarshalizerUtil.writeToJson(object, filename);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeReadFromJsonWithoutObject for readFromJson(random, null)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeReadFromJsonWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataMarshalizerUtil.readFromJson(filename, null);
    }

    @Test(expected = EmptyFilenameException.class)
    @TestCaseName("Run testNegativeReadFromJsonWithoutFilename for readFromJson({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesClassCaseData"
    )
    public void testNegativeReadFromJsonWithoutFilename(
            @NotNull final Class<Object> object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataMarshalizerUtil.readFromJson(filename, object);
    }

    @Test(expected = EmptyFileException.class)
    @TestCaseName("Run testNegativeReadFromJsonWithoutExistsFile for readFromJson({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    public void testNegativeReadFromJsonWithoutExistsFile(
            @NotNull final Class<Object> object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        DataMarshalizerUtil.readFromJson(filepath, object);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeWriteToXmlWithoutObject for writeToXml(null, random)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeWriteToXmlWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataMarshalizerUtil.writeToXml(null, filename);
    }

    @Test(expected = EmptyFilenameException.class)
    @TestCaseName("Run testNegativeWriteToXmlWithoutFilename for writeToXml({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesCaseData"
    )
    public void testNegativeWriteToXmlWithoutFilename(
            @NotNull final Object object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataMarshalizerUtil.writeToXml(object, filename);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeReadFromXmlWithoutObject for readFromXml(random, null)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeReadFromXmlWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataMarshalizerUtil.readFromXml(filename, null);
    }

    @Test(expected = EmptyFilenameException.class)
    @TestCaseName("Run testNegativeReadFromXmlWithoutFilename for readFromXml({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesClassCaseData"
    )
    public void testNegativeReadFromXmlWithoutFilename(
            @NotNull final Class<Object> object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataMarshalizerUtil.readFromXml(filename, object);
    }

    @Test(expected = EmptyFileException.class)
    @TestCaseName("Run testNegativeReadFromXmlWithoutExistsFile for readFromXml({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    public void testNegativeReadFromXmlWithoutExistsFile(
            @NotNull final Class<Object> object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        DataMarshalizerUtil.readFromXml(filepath, object);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeWriteToYamlWithoutObject for writeToYaml(null, random)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeWriteToYamlWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataMarshalizerUtil.writeToYaml(null, filename);
    }

    @Test(expected = EmptyFilenameException.class)
    @TestCaseName("Run testNegativeWriteToYamlWithoutFilename for writeToYaml({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesCaseData"
    )
    public void testNegativeWriteToYamlWithoutFilename(
            @NotNull final Object object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataMarshalizerUtil.writeToYaml(object, filename);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeReadFromYamlWithoutObject for readFromYaml(random, null)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeReadFromYamlWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataMarshalizerUtil.readFromYaml(filename, null);
    }

    @Test(expected = EmptyFilenameException.class)
    @TestCaseName("Run testNegativeReadFromYamlWithoutFilename for readFromYaml({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesClassCaseData"
    )
    public void testNegativeReadFromYamlWithoutFilename(
            @NotNull final Class<Object> object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataMarshalizerUtil.readFromYaml(filename, object);
    }

    @Test(expected = EmptyFileException.class)
    @TestCaseName("Run testNegativeReadFromYamlWithoutExistsFile for readFromYaml({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    public void testNegativeReadFromYamlWithoutExistsFile(
            @NotNull final Class<Object> object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        DataMarshalizerUtil.readFromYaml(filepath, object);
    }

    @Test
    @TestCaseName("Run testWriteToJson for writeToJson({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testWriteToJson(
            @NotNull final Object object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath() + ".json";
        Assert.assertNotNull(filepath);

        @NotNull final Object write = DataMarshalizerUtil.writeToJson(object, filepath);
        Assert.assertNotNull(write);
    }

    @Test
    @TestCaseName("Run testReadFromJson for readFromJson({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testReadFromJson(
            @NotNull final Class<Object> object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath() + ".json";
        Assert.assertNotNull(filepath);
        @NotNull final Object write = DataMarshalizerUtil.writeToJson(filename, filepath);
        Assert.assertNotNull(write);

        @NotNull final Object read = DataMarshalizerUtil.readFromJson(filepath, object);
        Assert.assertNotNull(read);
    }

    @Test
    @TestCaseName("Run testWriteToXml for writeToXml({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testWriteToXml(
            @NotNull final Object object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath() + ".xml";
        Assert.assertNotNull(filepath);

        @NotNull final Object write = DataMarshalizerUtil.writeToXml(object, filepath);
        Assert.assertNotNull(write);
    }

    @Test
    @TestCaseName("Run testReadFromXml for readFromXml({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testReadFromXml(
            @NotNull final Class<Object> object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath() + ".xml";
        Assert.assertNotNull(filepath);
        @NotNull final Object write = DataMarshalizerUtil.writeToXml(object, filepath);
        Assert.assertNotNull(write);

        @NotNull final Object read = DataMarshalizerUtil.readFromXml(filepath, object);
        Assert.assertNotNull(read);
    }

    @Test
    @TestCaseName("Run testWriteToYaml for writeToYaml({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testWriteToYaml(
            @NotNull final Object object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath() + ".yaml";
        Assert.assertNotNull(filepath);

        @NotNull final Object write = DataMarshalizerUtil.writeToYaml(object, filepath);
        Assert.assertNotNull(write);
    }

    @Test
    @TestCaseName("Run testReadFromYaml for readFromYaml({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testReadFromYaml(
            @Nullable final Class<Object> object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file.getAbsolutePath() + ".yaml";
        Assert.assertNotNull(filepath);
        @NotNull final Object write = DataMarshalizerUtil.writeToYaml(object, filepath);
        Assert.assertNotNull(write);

        @NotNull final Object read = DataMarshalizerUtil.readFromYaml(filepath, object);
        Assert.assertNotNull(read);
    }

}