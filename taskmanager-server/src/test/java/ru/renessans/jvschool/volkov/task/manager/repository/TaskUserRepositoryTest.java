package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyTaskException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class TaskUserRepositoryTest {

    @NotNull
    private final ITaskUserRepository taskUserRepository = new TaskUserRepository();

    @Test(expected = EmptyTaskException.class)
    @TestCaseName("Run testNegativeDeleteById for deleteById({0}, {1})")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testNegativeDeleteById(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);
        this.taskUserRepository.deleteById(user.getId(), task.getId() + ".");
    }

    @Test(expected = EmptyTaskException.class)
    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle({0}, {1})")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testNegativeDeleteByTitle(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);
        this.taskUserRepository.deleteByTitle(user.getId(), task.getTitle() + ".");
    }

    @Test
    @TestCaseName("Run testDeleteByIndex for deleteByIndex({0}, 0)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteByIndex(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Task deleteTask = this.taskUserRepository.deleteByIndex(user.getId(), 0);
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(task.getId(), deleteTask.getId());
        Assert.assertEquals(task.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(task.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(task.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteById for deleteById({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteById(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Task deleteTask = this.taskUserRepository.deleteById(user.getId(), task.getId());
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(task.getId(), deleteTask.getId());
        Assert.assertEquals(task.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(task.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(task.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteByTitle for deleteByTitle({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteByTitle(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Task deleteTask = this.taskUserRepository.deleteByTitle(user.getId(), task.getTitle());
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(task.getId(), deleteTask.getId());
        Assert.assertEquals(task.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(task.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(task.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteAll for deleteAll({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testDeleteAll(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Collection<Task> deleteTasks = this.taskUserRepository.deleteAll(user.getId());
        Assert.assertNotNull(deleteTasks);
        Assert.assertNotEquals(0, deleteTasks.size());
        final boolean isUserTasks = deleteTasks.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetAll for getAll({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetAll(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Collection<Task> getTasks = this.taskUserRepository.getAll(user.getId());
        Assert.assertNotNull(getTasks);
        Assert.assertNotEquals(0, getTasks.size());
        final boolean isUserTasks = getTasks.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetByIndex for getByIndex({0}, 0)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetByIndex(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Task getTask = this.taskUserRepository.getByIndex(user.getId(), 0);
        Assert.assertNotNull(getTask);
        Assert.assertEquals(task.getId(), getTask.getId());
        Assert.assertEquals(task.getUserId(), getTask.getUserId());
        Assert.assertEquals(task.getTitle(), getTask.getTitle());
        Assert.assertEquals(task.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetById for getById({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetById(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Task getTask = this.taskUserRepository.getById(user.getId(), task.getId());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(task.getId(), getTask.getId());
        Assert.assertEquals(task.getUserId(), getTask.getUserId());
        Assert.assertEquals(task.getTitle(), getTask.getTitle());
        Assert.assertEquals(task.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetByTitle for getByTitle({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetByTitle(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.taskUserRepository);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskUserRepository.addRecord(task);
        Assert.assertNotNull(addRecord);

        @Nullable final Task getTask = this.taskUserRepository.getByTitle(user.getId(), task.getTitle());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(task.getId(), getTask.getId());
        Assert.assertEquals(task.getUserId(), getTask.getUserId());
        Assert.assertEquals(task.getTitle(), getTask.getTitle());
        Assert.assertEquals(task.getDescription(), getTask.getDescription());
    }

}