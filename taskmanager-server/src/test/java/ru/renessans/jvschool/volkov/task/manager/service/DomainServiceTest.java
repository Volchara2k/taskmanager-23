package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDomainProvider;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.TaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepository;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class DomainServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITaskUserRepository taskRepository = new TaskUserRepository();

    @NotNull
    private final ITaskUserService taskService = new TaskUserService(taskRepository);

    @NotNull
    private final IProjectUserRepository projectRepository = new ProjectUserRepository();

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    @Test
    @TestCaseName("Run testDataImport for dataImport({0})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testDataImport(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(data);

        @Nullable final Domain importDomain = this.domainService.dataImport(data);
        Assert.assertNotNull(importDomain);
        @NotNull final Collection<User> importDomainUsers = importDomain.getUsers();
        Assert.assertNotNull(importDomainUsers);
        Assert.assertNotEquals(0, importDomainUsers.size());
        @NotNull final Collection<Task> importDomainTasks = importDomain.getTasks();
        Assert.assertNotNull(importDomainTasks);
        Assert.assertNotEquals(0, importDomainTasks.size());
        @NotNull final Collection<Project> importDomainProjects = importDomain.getProjects();
        Assert.assertNotNull(importDomainProjects);
        Assert.assertNotEquals(0, importDomainProjects.size());
        Assert.assertEquals(importDomainUsers, this.userService.getAllRecords());
        Assert.assertEquals(importDomainTasks, this.taskService.getAllRecords());
        Assert.assertEquals(importDomainProjects, this.projectService.getAllRecords());
    }

    @Test
    @TestCaseName("Run testDataExport for dataExport(domain)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testDataExport(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);

        @NotNull final Domain domain = new Domain();
        Assert.assertNotNull(domain);
        @NotNull final Domain exportDomain = this.domainService.dataExport(domain);
        Assert.assertNotNull(exportDomain);
        Assert.assertEquals(setAllUserRecords, exportDomain.getUsers());
        Assert.assertEquals(setAllTaskRecords, exportDomain.getTasks());
        Assert.assertEquals(setAllProjectRecords, exportDomain.getProjects());
    }

}