package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

public final class CaseDataProjectProvider {

    @SuppressWarnings("unused")
    public Object[] validProjectsCaseData() {
        @Nullable User user;
        return new Object[]{
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_TEST_LOGIN,
                                DemoDataConst.USER_TEST_PASSWORD
                        ),
                        new Project(
                                DemoDataConst.PROJECT_TITLE,
                                DemoDataConst.PROJECT_DESCRIPTION,
                                user.getId()
                        )
                },
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_DEFAULT_LOGIN,
                                DemoDataConst.USER_DEFAULT_PASSWORD,
                                DemoDataConst.USER_DEFAULT_FIRSTNAME
                        ),
                        new Project(
                                HashUtil.getSaltHashLine(DemoDataConst.PROJECT_TITLE),
                                HashUtil.getSaltHashLine(DemoDataConst.PROJECT_DESCRIPTION),
                                user.getId()
                        )
                },
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_ADMIN_LOGIN,
                                DemoDataConst.USER_ADMIN_PASSWORD,
                                UserRole.ADMIN
                        ),
                        new Project(
                                HashUtil.getHashLine(DemoDataConst.PROJECT_TITLE),
                                HashUtil.getHashLine(DemoDataConst.PROJECT_DESCRIPTION),
                                user.getId()
                        )
                }
        };
    }

}