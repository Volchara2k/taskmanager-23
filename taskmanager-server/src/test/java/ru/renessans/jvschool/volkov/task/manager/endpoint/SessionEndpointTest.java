package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ISessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataSessionProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class SessionEndpointTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IAuthenticationService authService = serviceLocator.getAuthenticationService();

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @Before
    public void loadConfigurationBefore() {
        Assert.assertNotNull(this.configService);
        this.configService.load();
    }

    @Test
    @TestCaseName("Run testOpenSession for openSession(\"{0}\",\"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testOpenSession(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addUser = this.userService.addUser(login, password);
        Assert.assertNotNull(addUser);

        @NotNull final Session open = this.sessionService.openSession(login, password);
        Assert.assertNotNull(open);
        Assert.assertEquals(addUser.getId(), open.getUserId());
        @Nullable final Session getSession = this.sessionService.getSessionByUserId(open);
        Assert.assertNotNull(getSession);
        Assert.assertEquals(open, getSession);
    }

    @Test
    @TestCaseName("Run testCloseSession for closeSession({1})")
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testCloseSession(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addUser);
        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(open);

        final boolean isClosedSession = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(isClosedSession);
    }

    @Test
    @TestCaseName("Run testValidateSession for validateSession({1})")
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testValidateSession(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addUser);
        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(open);

        @NotNull final Session validate = this.sessionEndpoint.validateSession(open);
        Assert.assertNotNull(validate);
        Assert.assertEquals(open.getId(), validate.getId());
        Assert.assertEquals(open.getUserId(), validate.getUserId());
        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
    }

    @Test
    @TestCaseName("Run testVerifyValidUserData for validateSession({1}, role)")
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testValidateSessionWithCommandRole(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash(), user.getRole());
        Assert.assertNotNull(addUser);
        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(open);

        @NotNull final Session validate = this.sessionEndpoint.validateSessionWithCommandRole(open, user.getRole());
        Assert.assertNotNull(validate);
        Assert.assertEquals(open.getId(), validate.getId());
        Assert.assertEquals(open.getUserId(), validate.getUserId());
        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
    }

    @Test
    @TestCaseName("Run testVerifyValidSessionState for validateSession({1})")
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testVerifyValidSessionState(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash(), user.getRole());
        Assert.assertNotNull(addUser);
        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(open);

        @NotNull final SessionValidState verifyValidSessionState = this.sessionEndpoint.verifyValidSessionState(open);
        Assert.assertNotNull(verifyValidSessionState);
        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
    }

    @Test
    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(\"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testVerifyValidPermissionState(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addUser = this.userService.addUser(login, password);
        Assert.assertNotNull(addUser);
        @NotNull final Session open = this.sessionService.openSession(login, password);
        Assert.assertNotNull(open);
        Assert.assertEquals(addUser.getId(), open.getUserId());

        @NotNull final PermissionValidState permissionValidState =
                this.sessionEndpoint.verifyValidPermissionState(open, addUser.getRole());
        Assert.assertNotNull(permissionValidState);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
    }

}