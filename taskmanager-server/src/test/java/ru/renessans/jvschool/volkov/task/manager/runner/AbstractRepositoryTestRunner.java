package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.repository.*;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                AbstractRepositoryTest.class,
                ProjectUserRepositoryTest.class,
                ServiceLocatorRepositoryTest.class,
                SessionRepositoryTest.class,
                TaskUserRepositoryTest.class,
                UserRepositoryTest.class
        }
)

public abstract class AbstractRepositoryTestRunner {
}