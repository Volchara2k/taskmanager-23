package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.service.*;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                AbstractServiceTest.class,
                AuthenticationServiceTest.class,
                DataInterChangeServiceTest.class,
                DomainServiceTest.class,
                ProjectUserServiceTest.class,
                ConfigurationServiceTest.class,
                ServiceLocatorServiceTest.class,
                SessionServiceTest.class,
                TaskUserServiceTest.class,
                UserServiceTest.class
        }
)

public abstract class AbstractServiceTestRunner {
}