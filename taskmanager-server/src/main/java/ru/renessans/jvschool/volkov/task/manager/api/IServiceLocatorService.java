package ru.renessans.jvschool.volkov.task.manager.api;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;

public interface IServiceLocatorService {

    @NotNull
    IUserService getUserService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    ITaskUserService getTaskService();

    @NotNull
    IProjectUserService getProjectService();

    @NotNull
    IDataInterChangeService getDataInterChangeService();

    @NotNull
    IConfigurationService getConfigurationService();

}