package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.file.EmptyFilenameException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@UtilityClass
public final class FileUtil {

    @SneakyThrows
    @NotNull
    public File create(@Nullable final String filename) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        delete(filename);
        @NotNull final File file = new File(filename);
        Files.createFile(file.toPath());

        return file;
    }

    @SneakyThrows
    public byte[] read(@Nullable final String filename) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();
        return Files.readAllBytes(Paths.get(filename));
    }

    @SneakyThrows
    public boolean delete(@Nullable final String filename) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) return false;
        if (isNotExists(filename)) return false;
        @NotNull final Path path = Paths.get(filename);
        return Files.deleteIfExists(path);
    }

    public boolean isNotExists(@Nullable final File file) {
        if (Objects.isNull(file)) return false;
        return !file.exists() || file.isDirectory();
    }

    public boolean isNotExists(@Nullable final String filename) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) return false;
        @NotNull final File file = new File(filename);
        return isNotExists(file);
    }

}