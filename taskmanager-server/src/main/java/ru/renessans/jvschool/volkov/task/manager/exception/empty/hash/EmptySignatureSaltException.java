package ru.renessans.jvschool.volkov.task.manager.exception.empty.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptySignatureSaltException extends AbstractException {

    @NotNull
    private static final String EMPTY_SALT = "Ошибка! Параметр \"соль для подписи\" является пустым или null!\n";

    public EmptySignatureSaltException() {
        super(EMPTY_SALT);
    }

}