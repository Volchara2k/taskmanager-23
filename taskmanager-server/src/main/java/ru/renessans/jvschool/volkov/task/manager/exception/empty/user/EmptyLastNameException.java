package ru.renessans.jvschool.volkov.task.manager.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyLastNameException extends AbstractException {

    @NotNull
    private static final String EMPTY_LAST_NAME = "Ошибка! Параметр \"фамилия\" является пустым или null!\n";

    public EmptyLastNameException() {
        super(EMPTY_LAST_NAME);
    }

}