package ru.renessans.jvschool.volkov.task.manager.exception.empty.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptySignatureObjectException extends AbstractException {

    @NotNull
    private static final String EMPTY_OBJECT = "Ошибка! Параметр \"объект для подписи\" является пустым или null!\n";

    public EmptySignatureObjectException() {
        super(EMPTY_OBJECT);
    }

}