package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @NotNull
    @SneakyThrows
    @Override
    public Project addProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.add(opened.getUserId(), title, description);
    }

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    @SneakyThrows
    @Override
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.updateByIndex(opened.getUserId(), index, title, description);
    }

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    @SneakyThrows
    @Override
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.updateById(opened.getUserId(), id, title, description);
    }

    @WebMethod
    @WebResult(name = "deletedProject", partName = "deletedProject")
    @Nullable
    @SneakyThrows
    @Override
    public Project deleteProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteById(opened.getUserId(), id);
    }

    @WebMethod
    @WebResult(name = "deletedProject", partName = "deletedProject")
    @Nullable
    @SneakyThrows
    @Override
    public Project deleteProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @WebResult(name = "deletedProject", partName = "deletedProject")
    @Nullable
    @SneakyThrows
    @Override
    public Project deleteProjectByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @WebResult(name = "deletedProjects", partName = "deletedProjects")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> deleteAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteAll(opened.getUserId());
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @SneakyThrows
    @Override
    public Project getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getById(opened.getUserId(), id);
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @SneakyThrows
    @Override
    public Project getProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @SneakyThrows
    @Override
    public Project getProjectByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getAll(opened.getUserId());
    }

}