package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import java.util.Collection;

public interface IProjectEndpoint {

    @NotNull
    Project addProject(
            @Nullable Session session,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    Project updateProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    Project updateProjectById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    Project deleteProjectById(
            @Nullable Session session,
            @Nullable String id
    );

    @Nullable
    Project deleteProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index
    );

    @Nullable
    Project deleteProjectByTitle(
            @Nullable Session session,
            @Nullable String title
    );

    @NotNull
    Collection<Project> deleteAllProjects(
            @Nullable Session session
    );

    @Nullable
    Project getProjectById(
            @Nullable Session session,
            @Nullable String id
    );

    @Nullable
    Project getProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index
    );

    @Nullable
    Project getProjectByTitle(
            @Nullable Session session,
            @Nullable String title
    );

    @NotNull
    Collection<Project> getAllProjects(
            @Nullable Session session
    );

}