package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyProjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class ProjectUserService extends AbstractService<Project> implements IProjectUserService {

    @NotNull
    private final IProjectUserRepository projectRepository;

    public ProjectUserService(
            @NotNull final IProjectUserRepository repository
    ) {
        super(repository);
        this.projectRepository = repository;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @NotNull final Project project = new Project(title, description);
        project.setUserId(userId);
        return super.addRecord(project);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();

        project.setTitle(title);
        project.setDescription(description);
        return super.updateRecord(project);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();

        project.setTitle(title);
        project.setDescription(description);
        return super.updateRecord(project);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.deleteByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        return this.projectRepository.deleteById(userId, id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        return this.projectRepository.deleteByTitle(userId, title);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> deleteAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.deleteAll(userId);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.getByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        return this.projectRepository.getById(userId, id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        return this.projectRepository.getByTitle(userId, title);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> getAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.getAll(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> initialDemoData(
            @Nullable final Collection<User> users
    ) {
        if (ValidRuleUtil.isNullOrEmpty(users)) throw new EmptyUserException();

        @NotNull final List<Project> demoData = new ArrayList<>();
        users.forEach(user -> {
            @NotNull final Project project =
                    add(user.getId(), DemoDataConst.PROJECT_TITLE, DemoDataConst.PROJECT_DESCRIPTION);
            demoData.add(project);
        });

        return demoData;
    }

}