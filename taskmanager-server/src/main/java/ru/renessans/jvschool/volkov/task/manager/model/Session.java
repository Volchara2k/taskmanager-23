package ru.renessans.jvschool.volkov.task.manager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractModel implements Cloneable {

    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    public Session(
            @NotNull final Long timestamp,
            @NotNull final String userId
    ) {
        this.timestamp = timestamp;
        this.userId = userId;
    }

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (final CloneNotSupportedException e) {
            return null;
        }
    }

}