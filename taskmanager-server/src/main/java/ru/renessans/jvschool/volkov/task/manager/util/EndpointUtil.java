package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint.EmptyEndpointException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint.EmptyEndpointsException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint.EmptyHostException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint.EmptyPortException;

import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@UtilityClass
public class EndpointUtil {

    @NotNull
    private final Logger logger = Logger.getLogger(EndpointUtil.class.getName());

    @NotNull
    @SneakyThrows
    public Endpoint publish(
            @Nullable final Object implementor,
            @Nullable final String host,
            @Nullable final Integer port
    ) {
        if (Objects.isNull(implementor)) throw new EmptyEndpointException();
        if (ValidRuleUtil.isNullOrEmpty(host)) throw new EmptyHostException();
        if (ValidRuleUtil.isNullOrEmpty(port)) throw new EmptyPortException();

        @NotNull final String name = implementor.getClass().getSimpleName();
        @NotNull final String address = "http://" + host + ":" + port + "/" + name + "?WSDL";
        logger.log(Level.INFO, address);

        @NotNull final Endpoint release = Endpoint.publish(address, implementor);
        return release;
    }

    @NotNull
    @SneakyThrows
    public Collection<Endpoint> publish(
            @Nullable final Collection<Object> implementors,
            @Nullable final String host,
            @Nullable final Integer port
    ) {
        if (ValidRuleUtil.isNullOrEmpty(implementors)) throw new EmptyEndpointsException();
        if (ValidRuleUtil.isNullOrEmpty(host)) throw new EmptyHostException();
        if (ValidRuleUtil.isNullOrEmpty(port)) throw new EmptyPortException();

        @NotNull final List<Endpoint> endpoints = new ArrayList<>();
        implementors.forEach(endpoint -> {
            @NotNull final Endpoint released = publish(endpoint, host, port);
            endpoints.add(released);
        });
        return endpoints;
    }

}