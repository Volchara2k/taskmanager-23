package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IAuthenticationEndpoint {

    @NotNull
    User signUpUser(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User signUpUserWithFirstName(
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

    @NotNull
    UserRole getUserRole(
            @Nullable Session session
    );

}