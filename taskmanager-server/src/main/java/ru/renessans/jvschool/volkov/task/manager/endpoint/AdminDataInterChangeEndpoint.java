package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public final class AdminDataInterChangeEndpoint extends AbstractEndpoint implements IAdminDataInterChangeEndpoint {

    @NotNull
    private static final UserRole ROLE = UserRole.ADMIN;

    public AdminDataInterChangeEndpoint() {
    }

    public AdminDataInterChangeEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "isClearedBinData", partName = "isClearedBinData")
    @SneakyThrows
    @Override
    public boolean dataBinClear(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataBinClear();
    }

    @WebMethod
    @WebResult(name = "isClearedBase64Data", partName = "isClearedBase64Data")
    @SneakyThrows
    @Override
    public boolean dataBase64Clear(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataBase64Clear();
    }

    @WebMethod
    @WebResult(name = "isClearedJsonData", partName = "isClearedJsonData")
    @SneakyThrows
    @Override
    public boolean dataJsonClear(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataJsonClear();
    }

    @WebMethod
    @WebResult(name = "isClearedXmlData", partName = "isClearedXmlData")
    @SneakyThrows
    @Override
    public boolean dataXmlClear(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataXmlClear();
    }

    @WebMethod
    @WebResult(name = "isClearedYamlData", partName = "isClearedYamlData")
    @SneakyThrows
    @Override
    public boolean dataYamlClear(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataYamlClear();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataBin();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataBase64();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataJson(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataJson();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataXml(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataXml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataYaml(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataYaml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataBin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataBin();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataBase64();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataJson(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataJson();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataXml(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataXml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataYaml(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataYaml();
    }

}