package ru.renessans.jvschool.volkov.task.manager.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.hash.EmptySignatureCycleException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.hash.EmptySignatureObjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.hash.EmptySignatureSaltException;

import java.util.Objects;

@UtilityClass
public class SignatureUtil {

    @NotNull
    @SneakyThrows
    public String getHashSignature(
            @Nullable final Object object,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (Objects.isNull(object)) throw new EmptySignatureObjectException();
        if (ValidRuleUtil.isNullOrEmpty(salt)) throw new EmptySignatureSaltException();
        if (ValidRuleUtil.isNullOrEmpty(cycle)) throw new EmptySignatureCycleException();

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(object);
        return getHashSignature(json, salt, cycle);
    }

    @NotNull
    @SneakyThrows
    public String getHashSignature(
            @Nullable final String line,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (ValidRuleUtil.isNullOrEmpty(line)) throw new EmptySignatureObjectException();
        if (ValidRuleUtil.isNullOrEmpty(salt)) throw new EmptySignatureSaltException();
        if (ValidRuleUtil.isNullOrEmpty(cycle)) throw new EmptySignatureCycleException();

        @NotNull String hashLine = line;
        for (int i = 0; i < cycle; i++) {
            hashLine = HashUtil.getHashLine(salt + hashLine + salt);
        }

        return hashLine;
    }

}