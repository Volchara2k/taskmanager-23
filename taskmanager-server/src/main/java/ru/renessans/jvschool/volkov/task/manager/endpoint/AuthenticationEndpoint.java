package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
public final class AuthenticationEndpoint extends AbstractEndpoint implements IAuthenticationEndpoint {

    public AuthenticationEndpoint() {
    }

    public AuthenticationEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @SneakyThrows
    @Override
    public User signUpUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        authService.verifyValidPermission(null, UserRole.UNKNOWN);
        return authService.signUp(login, password);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @SneakyThrows
    @Override
    public User signUpUserWithFirstName(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        authService.verifyValidPermission(null, UserRole.UNKNOWN);
        return authService.signUp(login, password, firstName);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @SneakyThrows
    @Override
    public UserRole getUserRole(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @Nullable final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(session)) {
            userId = session.getUserId();
        }
        return authService.getUserRole(userId);
    }

}