package ru.renessans.jvschool.volkov.task.manager.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    @NotNull
    private static final String EMPTY_EMAIL = "Ошибка! Параметр \"электронная почта\" является пустым или null!\n";

    public EmptyEmailException() {
        super(EMPTY_EMAIL);
    }

}