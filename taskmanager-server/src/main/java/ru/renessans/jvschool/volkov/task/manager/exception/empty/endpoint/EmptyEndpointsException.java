package ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyEndpointsException extends AbstractException {

    @NotNull
    private static final String EMPTY_ENDPOINTS = "Ошибка! Параметр \"endpoints\" является null!\n";

    public EmptyEndpointsException() {
        super(EMPTY_ENDPOINTS);
    }

}