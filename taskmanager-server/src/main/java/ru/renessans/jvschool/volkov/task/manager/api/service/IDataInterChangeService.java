package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;

public interface IDataInterChangeService {

    boolean dataBinClear();

    boolean dataBase64Clear();

    boolean dataJsonClear();

    boolean dataXmlClear();

    boolean dataYamlClear();

    @NotNull
    Domain exportDataBin();

    @NotNull
    Domain exportDataBase64();

    @NotNull
    Domain exportDataJson();

    @NotNull
    Domain exportDataXml();

    @NotNull
    Domain exportDataYaml();

    @NotNull
    Domain importDataBin();

    @NotNull
    Domain importDataBase64();

    @NotNull
    Domain importDataJson();

    @NotNull
    Domain importDataXml();

    @NotNull
    Domain importDataYaml();

}