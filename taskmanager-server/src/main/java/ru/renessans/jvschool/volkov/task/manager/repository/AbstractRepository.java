package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractRepository<E extends AbstractModel> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> recordMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public E addRecord(@NotNull final E value) {
        this.recordMap.put(value.getId(), value);
        return value;
    }

    @Nullable
    @Override
    public E updateRecord(@NotNull final E value) {
        @Nullable final E result = this.recordMap.putIfAbsent(value.getId(), value);
        return result;
    }

    @NotNull
    @Override
    public Collection<E> getAllRecords() {
        return new ArrayList<>(this.recordMap.values());
    }

    @Nullable
    @Override
    public E getRecordByKey(@NotNull final String key) {
        return this.recordMap.get(key);
    }

    @NotNull
    @Override
    public Collection<E> deleteAllRecords() {
        @NotNull final Collection<E> values = getAllRecords();
        this.recordMap.clear();
        return values;
    }

    @Nullable
    @Override
    public E deleteRecordByKey(@NotNull final String key) {
        @Nullable final E value = getRecordByKey(key);
        if (Objects.isNull(value)) return null;
        this.recordMap.remove(key);
        return value;
    }

    @Nullable
    @Override
    public E deleteRecord(@NotNull final E value) {
        this.recordMap.remove(value.getId());
        return value;
    }

    @NotNull
    @Override
    public Collection<E> setAllRecords(@NotNull final Collection<E> values) {
        values.forEach(this::addRecord);
        return new ArrayList<>(values);
    }

    @Override
    public boolean deletedRecord(@NotNull final E value) {
        @Nullable final E deleted = deleteRecord(value);
        return !Objects.isNull(deleted);
    }

    @Override
    public boolean deletedRecords() {
        @Nullable final Collection<E> deleted = deleteAllRecords();
        return !ValidRuleUtil.isNullOrEmpty(deleted);
    }

}