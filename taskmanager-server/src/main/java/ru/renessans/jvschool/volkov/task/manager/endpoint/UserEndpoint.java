package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IUserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
    }

    public UserEndpoint(
            @NotNull final IServiceLocatorService service
    ) {
        super(service);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public User getUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getRecordByKey(opened.getUserId());
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User editProfile(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(opened.getUserId(), firstName);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User editProfileWithLastName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(opened.getUserId(), firstName, lastName);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User updatePassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.updatePasswordById(opened.getUserId(), newPassword);
    }

}