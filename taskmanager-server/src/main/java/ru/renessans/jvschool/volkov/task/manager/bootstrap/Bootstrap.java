package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import java.util.Arrays;
import java.util.Collection;

public final class Bootstrap {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);


    @NotNull
    private final IAuthenticationEndpoint authEndpoint = new AuthenticationEndpoint(serviceLocator);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(serviceLocator);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final IAdminDataInterChangeEndpoint dataEndpoint = new AdminDataInterChangeEndpoint(serviceLocator);

    public void run() {
        initialConfiguration();
        initialWebServices();
        initialDemoData();
    }

    private void initialConfiguration() {
        @NotNull final IConfigurationService configService = this.serviceLocator.getConfigurationService();
        configService.load();
    }

    private void initialWebServices() {
        @NotNull final IConfigurationService configService = this.serviceLocator.getConfigurationService();
        @NotNull final String host = configService.getServerHost();
        @NotNull final Integer port = configService.getServerPort();
        EndpointUtil.publish(endpointList(), host, port);
    }

    private Collection<Object> endpointList() {
        return Arrays.asList(
                sessionEndpoint, authEndpoint, adminEndpoint, userEndpoint,
                taskEndpoint, projectEndpoint, dataEndpoint
        );
    }

    private void initialDemoData() {
        @NotNull final IUserService userService = this.serviceLocator.getUserService();
        @NotNull final Collection<User> users = userService.initialDemoData();
        @NotNull final ITaskUserService taskService = this.serviceLocator.getTaskService();
        taskService.initialDemoData(users);
        @NotNull final IProjectUserService projectService = this.serviceLocator.getProjectService();
        projectService.initialDemoData(users);
    }

}