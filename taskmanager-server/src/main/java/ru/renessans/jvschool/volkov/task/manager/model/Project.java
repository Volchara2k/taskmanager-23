package ru.renessans.jvschool.volkov.task.manager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractModel {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String title = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId;

    public Project(
            @NotNull final String title,
            @NotNull final String description
    ) {
        this.title = title;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return "Заголовок проекта: " + this.title +
                ", описание проекта: " + this.description +
                "\nИдентификатор: " + super.id + "\n";
    }

}