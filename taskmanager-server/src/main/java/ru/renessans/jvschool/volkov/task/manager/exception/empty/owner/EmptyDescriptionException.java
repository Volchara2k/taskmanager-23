package ru.renessans.jvschool.volkov.task.manager.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyDescriptionException extends AbstractException {

    @NotNull
    private static final String EMPTY_DESCRIPTION = "Ошибка! Параметр \"описание\" является пустым или null!\n";

    public EmptyDescriptionException() {
        super(EMPTY_DESCRIPTION);
    }

}