package ru.renessans.jvschool.volkov.task.manager.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.file.EmptyFilenameException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyKeyException;

import java.io.File;
import java.util.Objects;

@UtilityClass
public class DataMarshalizerUtil {

    @SneakyThrows
    public <T> T writeToJson(
            @Nullable final T t,
            @Nullable final String filename
    ) {
        if (Objects.isNull(t)) throw new EmptyKeyException();
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File file = FileUtil.create(filename);

        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromJson(
            @Nullable final String filename,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();
        if (Objects.isNull(tClass)) throw new EmptyKeyException();
        if (FileUtil.isNotExists(filename)) throw new EmptyFileException();

        @NotNull final File file = new File(filename);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> T writeToXml(
            @Nullable final T t,
            @Nullable final String filename
    ) {
        if (Objects.isNull(t)) throw new EmptyKeyException();
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File file = FileUtil.create(filename);

        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromXml(
            @Nullable final String filename,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();
        if (Objects.isNull(tClass)) throw new EmptyKeyException();
        if (FileUtil.isNotExists(filename)) throw new EmptyFileException();

        @NotNull final File file = new File(filename);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();

        return xmlMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> T writeToYaml(
            @Nullable final T t,
            @Nullable final String filename
    ) {
        if (Objects.isNull(t)) throw new EmptyKeyException();
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper(yamlFactory);
        @NotNull final File file = FileUtil.create(filename);

        yamlMapper.writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromYaml(
            @Nullable final String filename,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();
        if (Objects.isNull(tClass)) throw new EmptyKeyException();
        if (FileUtil.isNotExists(filename)) throw new EmptyFileException();

        @NotNull final File file = new File(filename);
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper(yamlFactory);

        return yamlMapper.readValue(file, tClass);
    }

}