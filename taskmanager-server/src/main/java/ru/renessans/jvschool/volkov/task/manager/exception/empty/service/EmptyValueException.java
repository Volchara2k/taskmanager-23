package ru.renessans.jvschool.volkov.task.manager.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyValueException extends AbstractException {

    @NotNull
    private static final String EMPTY_VALUE = "Ошибка! Параметр \"значение\" является пустым или null!\n";

    public EmptyValueException() {
        super(EMPTY_VALUE);
    }

}