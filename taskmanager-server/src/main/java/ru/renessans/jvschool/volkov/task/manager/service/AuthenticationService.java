package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.user.*;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

public final class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    public AuthenticationService(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public UserRole getUserRole(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) return UserRole.UNKNOWN;
        @Nullable final User user = this.userService.getRecordByKey(userId);
        if (Objects.isNull(user)) return UserRole.UNKNOWN;
        return user.getRole();
    }

    @NotNull
    @SneakyThrows
    @Override
    public PermissionValidState verifyValidPermission(
            @Nullable final String userId,
            @Nullable final UserRole requiredRole
    ) {
        if (Objects.isNull(requiredRole)) return PermissionValidState.SUCCESS;

        @NotNull final UserRole userRole = getUserRole(userId);

        final boolean isOnlyUnknownCommand = requiredRole.equals(UserRole.UNKNOWN);
        final boolean isAuthedUserRole = (userRole != UserRole.UNKNOWN);
        if (isOnlyUnknownCommand && isAuthedUserRole) return PermissionValidState.LOGGED;

        boolean containsTypes = requiredRole.equals(userRole);
        if (containsTypes) return PermissionValidState.SUCCESS;

        final boolean isOnlyAdminCommand = requiredRole.equals(UserRole.ADMIN);
        final boolean isUserUserRole = (userRole == UserRole.USER);
        if (isOnlyAdminCommand && isUserUserRole) return PermissionValidState.NO_ACCESS_RIGHTS;

        return PermissionValidState.NEED_LOG_IN;
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserDataValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return UserDataValidState.USER_NOT_FOUND;
        if (user.getLockdown()) return UserDataValidState.LOCKDOWN_PROFILE;

        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        final boolean isNotEqualPasswords = !passwordHash.equals(user.getPasswordHash());
        if (isNotEqualPasswords) return UserDataValidState.INVALID_PASSWORD;

        return UserDataValidState.SUCCESS;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        return this.userService.addUser(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();
        return this.userService.addUser(login, password, firstName);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole role
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();
        return this.userService.addUser(login, password, role);
    }

}