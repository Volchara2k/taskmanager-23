package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.service.*;

public final class ServiceLocatorRepository implements IServiceLocatorRepository {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);


    @NotNull
    private final ITaskUserRepository taskRepository = new TaskUserRepository();

    @NotNull
    private final ITaskUserService taskService = new TaskUserService(taskRepository);


    @NotNull
    private final IProjectUserRepository projectRepository = new ProjectUserRepository();

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(projectRepository);


    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService
    );

    @NotNull
    private final IDataInterChangeService dataService = new DataInterChangeService(
            configService, domainService
    );


    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(
            sessionRepository, authService, userService, configService
    );

    @NotNull
    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return this.sessionService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.authService;
    }

    @NotNull
    @Override
    public ITaskUserService getTaskService() {
        return this.taskService;
    }

    @NotNull
    @Override
    public IProjectUserService getProjectService() {
        return this.projectService;
    }

    @NotNull
    @Override
    public IDataInterChangeService getDataInterChangeService() {
        return this.dataService;
    }

    @NotNull
    @Override
    public IConfigurationService getConfigurationService() {
        return this.configService;
    }

}