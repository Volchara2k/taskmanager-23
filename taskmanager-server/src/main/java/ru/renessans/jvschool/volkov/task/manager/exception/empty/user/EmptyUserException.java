package ru.renessans.jvschool.volkov.task.manager.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyUserException extends AbstractException {

    @NotNull
    private static final String EMPTY_USER =
            "Ошибка! Параметр \"пользователь\" является null!\n";

    public EmptyUserException() {
        super(EMPTY_USER);
    }

}