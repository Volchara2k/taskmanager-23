package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;

public interface IDomainService {

    Domain dataImport(@Nullable Domain domain);

    Domain dataExport(@Nullable Domain domain);

}