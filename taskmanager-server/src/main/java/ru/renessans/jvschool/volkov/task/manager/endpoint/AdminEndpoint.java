package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    private static final UserRole ROLE = UserRole.ADMIN;

    public AdminEndpoint() {
    }

    public AdminEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "isClosedAllSessions", partName = "isClosedAllSessions")
    @SneakyThrows
    @Override
    public boolean closeAllSessions(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        return sessionService.closeAllSessions(session);
    }

    @WebMethod
    @WebResult(name = "sessions", partName = "sessions")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Session> getAllSessions(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        return sessionService.getAllRecords();
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @SneakyThrows
    @Override
    public User signUpUserWithUserRole(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "userRole", partName = "userRole") @Nullable final UserRole role
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        return authService.signUp(login, password, role);
    }

    @WebMethod
    @WebResult(name = "deletedUser", partName = "deletedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User deleteUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.deleteRecordByKey(id);
    }

    @WebMethod
    @WebResult(name = "deletedUser", partName = "deletedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User deleteUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.deleteUserByLogin(login);
    }

    @WebMethod
    @WebResult(name = "deletedUsers", partName = "deletedUsers")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<User> deleteAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.deleteAllRecords();
    }

    @WebMethod
    @WebResult(name = "lockedUser", partName = "lockedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.lockUserByLogin(login);
    }

    @WebMethod
    @WebResult(name = "unlockedUser", partName = "unlockedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.unlockUserByLogin(login);
    }

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<User> setAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "users", partName = "users") @Nullable final Collection<User> values
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.setAllRecords(values);
    }

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<User> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getAllRecords();
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> getAllUsersTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getAllRecords();
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> setAllUsersTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "tasks", partName = "tasks") @Nullable final Collection<Task> values
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.setAllRecords(values);
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> getAllUsersProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getAllRecords();
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> setAllUsersProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projects", partName = "projects") @Nullable final Collection<Project> values
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.setAllRecords(values);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public User getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getRecordByKey(id);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public User getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getUserByLogin(login);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User editProfileById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(id, firstName);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User editProfileByIdWithLastName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(id, firstName, lastName);
    }

    @WebMethod
    @WebResult(name = "updatedUser", partName = "updatedUser")
    @Nullable
    @SneakyThrows
    @Override
    public User updatePasswordById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.updatePasswordById(id, newPassword);
    }

}