package ru.renessans.jvschool.volkov.task.manager.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyTitleException extends AbstractException {

    @NotNull
    private static final String EMPTY_TITLE = "Ошибка! Параметр \"заголовок\" является пустым или null!\n";

    public EmptyTitleException() {
        super(EMPTY_TITLE);
    }

}