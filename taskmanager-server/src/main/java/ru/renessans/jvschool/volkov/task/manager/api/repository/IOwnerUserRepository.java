package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.Collection;

public interface IOwnerUserRepository<E extends AbstractModel> extends IRepository<E> {

    @Nullable
    E deleteByIndex(
            @NotNull String userId,
            @NotNull Integer index
    );

    @Nullable
    E deleteById(
            @NotNull String userId,
            @NotNull String id
    );

    @Nullable
    E deleteByTitle(
            @NotNull String userId,
            @NotNull String title
    );

    @NotNull
    Collection<E> deleteAll(
            @NotNull String userId
    );

    @NotNull
    Collection<E> getAll(
            @NotNull String userId
    );

    @Nullable
    E getByIndex(
            @NotNull String userId,
            @NotNull Integer index
    );

    @Nullable
    E getById(
            @NotNull String userId,
            @NotNull String id
    );

    @Nullable
    E getByTitle(
            @NotNull String userId,
            @NotNull String title
    );

}