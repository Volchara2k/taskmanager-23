package ru.renessans.jvschool.volkov.task.manager.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    @NotNull
    private static final String EMPTY_LOGIN = "Ошибка! Параметр \"логин\" является пустым или null!\n";

    public EmptyLoginException() {
        super(EMPTY_LOGIN);
    }

}