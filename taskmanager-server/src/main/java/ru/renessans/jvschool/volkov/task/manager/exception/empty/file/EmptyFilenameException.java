package ru.renessans.jvschool.volkov.task.manager.exception.empty.file;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyFilenameException extends AbstractException {

    @NotNull
    private static final String EMPTY_FILENAME = "Ошибка! Параметр \"название файла\" является null!\n";

    public EmptyFilenameException() {
        super(EMPTY_FILENAME);
    }

}