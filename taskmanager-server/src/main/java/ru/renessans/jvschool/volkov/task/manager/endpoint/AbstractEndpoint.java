package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;

@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected IServiceLocatorService serviceLocator;

}