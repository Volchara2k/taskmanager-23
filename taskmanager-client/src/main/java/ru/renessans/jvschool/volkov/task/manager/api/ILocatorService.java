package ru.renessans.jvschool.volkov.task.manager.api;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;

public interface ILocatorService {

    @NotNull
    IEndpointLocatorService getEndpointLocator();

    @NotNull
    IServiceLocatorService getServiceLocator();

}