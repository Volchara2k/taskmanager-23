package ru.renessans.jvschool.volkov.task.manager.command.admin.data.yaml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataYamlClearCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_YAML_CLEAR = "data-yaml-clear";

    @NotNull
    private static final String DESC_YAML_CLEAR = "очистить yaml данные";

    @NotNull
    private static final String NOTIFY_YAML_CLEAR = "Происходит процесс очищения yaml данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_YAML_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_YAML_CLEAR;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        final boolean removeState = dataEndpoint.dataYamlClear(open);
        ViewUtil.print(NOTIFY_YAML_CLEAR);
        ViewUtil.print(removeState);
    }

}