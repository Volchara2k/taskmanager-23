package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalInstantiationException;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private static final String COMMAND_PACKAGE = "ru.renessans.jvschool.volkov.task.manager.command";

    @NotNull
    private static final List<AbstractCommand> COMMANDS = new ArrayList<>();

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> argumentMap = new LinkedHashMap<>();

    {
        initialCommands();
        getAllTerminalCommands().forEach(command -> commandMap.put(command.getCommand(), command));
        getAllArgumentCommands().forEach(command -> argumentMap.put(command.getArgument(), command));
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return COMMANDS;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return COMMANDS
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.getCommand()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return COMMANDS
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.getArgument()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public AbstractCommand getTerminalCommand(@NotNull final String command) {
        return this.commandMap.values()
                .stream()
                .filter(cmd -> command.equals(cmd.getCommand()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public AbstractCommand getArgumentCommand(@NotNull final String command) {
        return this.argumentMap.values()
                .stream()
                .filter(cmd -> command.equals(cmd.getArgument()))
                .findAny()
                .orElse(null);
    }

    @NotNull
    private Set<Class<? extends AbstractCommand>> extractCommandSet() {
        @NotNull final Reflections reflections = new Reflections(COMMAND_PACKAGE);
        @NotNull final Set<Class<? extends AbstractCommand>> aClasses = reflections.getSubTypesOf(AbstractCommand.class);
        return aClasses;
    }


    private void initialCommands() {
        @NotNull final Set<Class<? extends AbstractCommand>> classes = extractCommandSet();
        classes.forEach(aClass -> {
            final boolean isAbstractCommand = Modifier.isAbstract(aClass.getModifiers());
            if (isAbstractCommand) return;
            try {
                COMMANDS.add(aClass.newInstance());
            } catch (@NotNull final Exception e) {
                throw new IllegalInstantiationException();
            }
        });
    }

}