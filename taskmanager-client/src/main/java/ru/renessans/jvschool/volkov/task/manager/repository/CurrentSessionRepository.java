package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptySessionException;

import java.util.Objects;

public final class CurrentSessionRepository implements ICurrentSessionRepository {

    @Nullable
    private Session session;

    @NotNull
    @Override
    public Session put(@NotNull final Session session) {
        this.session = session;
        return session;
    }

    @Nullable
    @Override
    public Session get() {
        return this.session;
    }

    @SneakyThrows
    @Override
    public Session delete() {
        @Nullable final Session deleted = get();
        if (Objects.isNull(deleted)) throw new EmptySessionException();
        this.session = null;
        return deleted;
    }

}