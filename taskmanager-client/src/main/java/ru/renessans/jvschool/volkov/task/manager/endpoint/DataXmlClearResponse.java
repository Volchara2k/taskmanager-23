package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataXmlClearResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="dataXmlClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isClearedXmlData" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataXmlClearResponse", propOrder = {
        "isClearedXmlData"
})
public class DataXmlClearResponse {

    protected boolean isClearedXmlData;

    /**
     * Gets the value of the isClearedXmlData property.
     */
    public boolean isIsClearedXmlData() {
        return isClearedXmlData;
    }

    /**
     * Sets the value of the isClearedXmlData property.
     */
    public void setIsClearedXmlData(boolean value) {
        this.isClearedXmlData = value;
    }

}
