package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Task;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskDeleteByIndexCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_INDEX = "task-delete-by-index";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_INDEX = "удалить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_INDEX =
            "Происходит попытка инициализации удаления задачи. \n" +
                    "Для удаления задачи по индексу введите индекс задачи из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_DELETE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final Task task = taskEndpoint.deleteTaskByIndex(open, index);
        ViewUtil.print(task);
    }

}