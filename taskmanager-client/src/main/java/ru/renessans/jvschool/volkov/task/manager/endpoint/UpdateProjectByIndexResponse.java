
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateProjectByIndexResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateProjectByIndexResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="updatedProject" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}project" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateProjectByIndexResponse", propOrder = {
    "updatedProject"
})
public class UpdateProjectByIndexResponse {

    protected Project updatedProject;

    /**
     * Gets the value of the updatedProject property.
     * 
     * @return
     *     possible object is
     *     {@link Project }
     *     
     */
    public Project getUpdatedProject() {
        return updatedProject;
    }

    /**
     * Sets the value of the updatedProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Project }
     *     
     */
    public void setUpdatedProject(Project value) {
        this.updatedProject = value;
    }

}
