package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-16T17:03:40.783+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteProjectByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteProjectByIdResponse")
    @RequestWrapper(localName = "deleteProjectById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteProjectById")
    @ResponseWrapper(localName = "deleteProjectByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteProjectByIdResponse")
    @WebResult(name = "deletedProject", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project deleteProjectById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getAllProjectsRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getAllProjectsResponse")
    @RequestWrapper(localName = "getAllProjects", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllProjects")
    @ResponseWrapper(localName = "getAllProjectsResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetAllProjectsResponse")
    @WebResult(name = "projects", targetNamespace = "")
    java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.Project> getAllProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteProjectByTitleRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteProjectByTitleResponse")
    @RequestWrapper(localName = "deleteProjectByTitle", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteProjectByTitle")
    @ResponseWrapper(localName = "deleteProjectByTitleResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteProjectByTitleResponse")
    @WebResult(name = "deletedProject", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project deleteProjectByTitle(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "title", targetNamespace = "")
                    java.lang.String title
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getProjectByTitleRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getProjectByTitleResponse")
    @RequestWrapper(localName = "getProjectByTitle", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetProjectByTitle")
    @ResponseWrapper(localName = "getProjectByTitleResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetProjectByTitleResponse")
    @WebResult(name = "project", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project getProjectByTitle(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "title", targetNamespace = "")
                    java.lang.String title
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteAllProjectsRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteAllProjectsResponse")
    @RequestWrapper(localName = "deleteAllProjects", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteAllProjects")
    @ResponseWrapper(localName = "deleteAllProjectsResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteAllProjectsResponse")
    @WebResult(name = "deletedProjects", targetNamespace = "")
    java.util.List<ru.renessans.jvschool.volkov.task.manager.endpoint.Project> deleteAllProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getProjectByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getProjectByIdResponse")
    @RequestWrapper(localName = "getProjectById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetProjectById")
    @ResponseWrapper(localName = "getProjectByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetProjectByIdResponse")
    @WebResult(name = "project", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project getProjectById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteProjectByIndexRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/deleteProjectByIndexResponse")
    @RequestWrapper(localName = "deleteProjectByIndex", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteProjectByIndex")
    @ResponseWrapper(localName = "deleteProjectByIndexResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.DeleteProjectByIndexResponse")
    @WebResult(name = "deletedProject", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project deleteProjectByIndex(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getProjectByIndexRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/getProjectByIndexResponse")
    @RequestWrapper(localName = "getProjectByIndex", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetProjectByIndex")
    @ResponseWrapper(localName = "getProjectByIndexResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.GetProjectByIndexResponse")
    @WebResult(name = "project", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project getProjectByIndex(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UpdateProjectByIndexResponse")
    @WebResult(name = "updatedProject", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project updateProjectByIndex(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index,
            @WebParam(name = "title", targetNamespace = "")
                    java.lang.String title,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.UpdateProjectByIdResponse")
    @WebResult(name = "updatedProject", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project updateProjectById(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "title", targetNamespace = "")
                    java.lang.String title,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/addProjectRequest", output = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/ProjectEndpoint/addProjectResponse")
    @RequestWrapper(localName = "addProject", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.AddProject")
    @ResponseWrapper(localName = "addProjectResponse", targetNamespace = "http://endpoint.manager.task.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.task.manager.endpoint.AddProjectResponse")
    @WebResult(name = "project", targetNamespace = "")
    ru.renessans.jvschool.volkov.task.manager.endpoint.Project addProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.renessans.jvschool.volkov.task.manager.endpoint.Session session,
            @WebParam(name = "title", targetNamespace = "")
                    java.lang.String title,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );
}
