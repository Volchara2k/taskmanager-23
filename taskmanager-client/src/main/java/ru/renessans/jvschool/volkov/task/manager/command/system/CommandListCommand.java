package ru.renessans.jvschool.volkov.task.manager.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@SuppressWarnings("unused")
public final class CommandListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_COMMANDS = "commands";

    @NotNull
    private static final String ARG_COMMANDS = "-cmd";

    @NotNull
    private static final String DESC_COMMANDS = "вывод списка поддерживаемых терминальных команд";

    @NotNull
    private static final String NOTIFY_COMMANDS = "Список поддерживаемых терминальных команд: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_COMMANDS;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_COMMANDS;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_COMMANDS;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();

        @Nullable final Collection<AbstractCommand> commands = commandService.getAllTerminalCommands();
        ViewUtil.print(NOTIFY_COMMANDS);
        ViewUtil.print(commands);
    }

}