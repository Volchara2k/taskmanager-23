package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for user complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="user"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}abstractModel"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lockdown" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="role" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}userRole" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = {
        "firstName",
        "lastName",
        "lockdown",
        "login",
        "middleName",
        "passwordHash",
        "role"
})
public class User
        extends AbstractModel {

    protected String firstName;
    protected String lastName;
    protected Boolean lockdown;
    protected String login;
    protected String middleName;
    protected String passwordHash;
    @XmlSchemaType(name = "string")
    protected UserRole role;

    /**
     * Gets the value of the firstName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the lockdown property.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isLockdown() {
        return lockdown;
    }

    /**
     * Sets the value of the lockdown property.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setLockdown(Boolean value) {
        this.lockdown = value;
    }

    /**
     * Gets the value of the login property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLogin(String value) {
        this.login = value;
    }

    /**
     * Gets the value of the middleName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the passwordHash property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets the value of the passwordHash property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPasswordHash(String value) {
        this.passwordHash = value;
    }

    /**
     * Gets the value of the role property.
     *
     * @return possible object is
     * {@link UserRole }
     */
    public UserRole getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     *
     * @param value allowed object is
     *              {@link UserRole }
     */
    public void setRole(UserRole value) {
        this.role = value;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Логин: ").append(login);
        if (this.firstName != null && !this.firstName.equals(""))
            result.append(", имя: ").append(this.firstName).append("\n");
        if (this.lastName != null && !this.lastName.equals(""))
            result.append(", фамилия: ").append(this.lastName).append("\n");
        result.append("\nРоль: ").append(this.role).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}