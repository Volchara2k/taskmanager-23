package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataBinClearResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="dataBinClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isClearedBinData" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataBinClearResponse", propOrder = {
        "isClearedBinData"
})
public class DataBinClearResponse {

    protected boolean isClearedBinData;

    /**
     * Gets the value of the isClearedBinData property.
     */
    public boolean isIsClearedBinData() {
        return isClearedBinData;
    }

    /**
     * Sets the value of the isClearedBinData property.
     */
    public void setIsClearedBinData(boolean value) {
        this.isClearedBinData = value;
    }

}
