package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Project;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_PROJECT_CREATE = "project-create";

    @NotNull
    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    @NotNull
    private static final String NOTIFY_PROJECT_CREATE =
            "Происходит попытка инициализации создания проекта. \n" +
                    "Для создания проекта введите его заголовок или заголовок с описанием. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_CREATE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_CREATE;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_PROJECT_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.addProject(open, title, description);
        ViewUtil.print(project);
    }

}