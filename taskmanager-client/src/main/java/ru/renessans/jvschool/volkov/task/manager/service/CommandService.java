package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.ILocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.command.EmptyCommandException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyLocatorException;

import java.util.Collection;
import java.util.Objects;

@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @NotNull
    @SneakyThrows
    @Override
    public Collection<AbstractCommand> initialCommands(
            @Nullable final ILocatorService locatorService
    ) {
        if (Objects.isNull(locatorService)) throw new EmptyLocatorException();
        @NotNull final Collection<AbstractCommand> commands = this.commandRepository.getAllCommands();
        commands.forEach(command -> command.setLocatorService(locatorService));
        return commands;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return this.commandRepository.getAllCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return this.commandRepository.getAllTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return this.commandRepository.getAllArgumentCommands();
    }

    @Nullable
    @SneakyThrows
    @Override
    public AbstractCommand getTerminalCommand(
            @Nullable final String command
    ) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getTerminalCommand(command);
    }

    @Nullable
    @SneakyThrows
    @Override
    public AbstractCommand getArgumentCommand(
            @Nullable final String command
    ) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getArgumentCommand(command);
    }

}