package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataJsonClearResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="dataJsonClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isClearedJsonData" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataJsonClearResponse", propOrder = {
        "isClearedJsonData"
})
public class DataJsonClearResponse {

    protected boolean isClearedJsonData;

    /**
     * Gets the value of the isClearedJsonData property.
     */
    public boolean isIsClearedJsonData() {
        return isClearedJsonData;
    }

    /**
     * Sets the value of the isClearedJsonData property.
     */
    public void setIsClearedJsonData(boolean value) {
        this.isClearedJsonData = value;
    }

}
