
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteProjectByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteProjectByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedProject" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}project" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteProjectByIdResponse", propOrder = {
    "deletedProject"
})
public class DeleteProjectByIdResponse {

    protected Project deletedProject;

    /**
     * Gets the value of the deletedProject property.
     * 
     * @return
     *     possible object is
     *     {@link Project }
     *     
     */
    public Project getDeletedProject() {
        return deletedProject;
    }

    /**
     * Sets the value of the deletedProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Project }
     *     
     */
    public void setDeletedProject(Project value) {
        this.deletedProject = value;
    }

}
