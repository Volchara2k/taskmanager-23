package ru.renessans.jvschool.volkov.task.manager.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyOwnerUserException extends AbstractException {

    @NotNull
    private static final String EMPTY_ABSTRACT_MODEL =
            "Ошибка! Параметр \"искомая сущность пользователя\" является null!\n";

    public EmptyOwnerUserException() {
        super(EMPTY_ABSTRACT_MODEL);
    }

}