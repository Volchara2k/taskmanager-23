package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Project;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProjectViewByTitleCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_TITLE = "project-view-by-title";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_TITLE = "просмотреть проект по заголовку";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_TITLE =
            "Происходит попытка инициализации отображения проекта. \n" +
                    "Для отображения проекта по заголовку введите заголовок проекта из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.getProjectByTitle(open, title);
        ViewUtil.print(project);
    }

}