package ru.renessans.jvschool.volkov.task.manager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.endpoint.User;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProfileEditCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_EDIT_PROFILE = "edit-profile";

    @NotNull
    private static final String DESC_EDIT_PROFILE = "изменить данные пользователя";

    @NotNull
    private static final String NOTIFY_EDIT_PROFILE =
            "Происходит попытка инициализации редактирования данных пользователя. \n" +
                    "Для обновления данных пользователя введите его имя или имя с фамилией. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_EDIT_PROFILE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_EDIT_PROFILE;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSession = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSession.getSession();

        ViewUtil.print(NOTIFY_EDIT_PROFILE);
        @NotNull final String firstName = ViewUtil.getLine();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.editProfile(open, firstName);
        ViewUtil.print(user);
    }

}