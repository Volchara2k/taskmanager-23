package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Task;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskDeleteByTitleCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_TITLE = "task-delete-by-title";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_TITLE = "удалить задачу по заголовку";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_TITLE =
            "Происходит попытка инициализации удаления задачи. \n" +
                    "Для удаления задачи по заголовку введите имя заголовок из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_DELETE_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final Task task = taskEndpoint.deleteTaskByTitle(open, title);
        ViewUtil.print(task);
    }

}