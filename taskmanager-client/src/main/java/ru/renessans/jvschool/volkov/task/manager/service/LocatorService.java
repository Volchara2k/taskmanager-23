package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.ILocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;

@AllArgsConstructor
public final class LocatorService implements ILocatorService {

    @NotNull
    private final IEndpointLocatorService endpointLocatorService;

    @NotNull
    private final IServiceLocatorService serviceLocatorService;

    @NotNull
    @Override
    public IEndpointLocatorService getEndpointLocator() {
        return this.endpointLocatorService;
    }

    @NotNull
    @Override
    public IServiceLocatorService getServiceLocator() {
        return this.serviceLocatorService;
    }

}