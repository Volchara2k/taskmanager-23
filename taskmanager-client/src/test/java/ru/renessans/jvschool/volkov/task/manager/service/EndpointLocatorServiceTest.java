package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;

@Category(IntegrationImplementation.class)
public final class EndpointLocatorServiceTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @Test
    @TestCaseName("Run testGetAuthenticationEndpoint for getAuthenticationEndpoint()")
    public void testGetAuthenticationEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final AuthenticationEndpoint endpoint = this.endpointLocator.getAuthenticationEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetAdminEndpoint for getAdminEndpoint()")
    public void testGetAdminEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final AdminEndpoint endpoint = this.endpointLocator.getAdminEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetAdminDataInterChangeEndpoint for getAdminDataInterChangeEndpoint()")
    public void testGetAdminDataInterChangeEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final AdminDataInterChangeEndpoint endpoint = this.endpointLocator.getAdminDataInterChangeEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetSessionEndpoint for getSessionEndpoint()")
    public void testGetSessionEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final SessionEndpoint endpoint = this.endpointLocator.getSessionEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetUserEndpoint for getUserEndpoint()")
    public void testGetUserEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final UserEndpoint endpoint = this.endpointLocator.getUserEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetProjectEndpoint for getProjectEndpoint()")
    public void testGetProjectEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final ProjectEndpoint endpoint = this.endpointLocator.getProjectEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetTaskEndpoint for getTaskEndpoint()")
    public void testGetTaskEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        @NotNull final TaskEndpoint endpoint = this.endpointLocator.getTaskEndpoint();
        Assert.assertNotNull(endpoint);
    }

}