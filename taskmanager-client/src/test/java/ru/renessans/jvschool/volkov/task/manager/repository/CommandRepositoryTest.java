package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

import java.util.Collection;

public final class CommandRepositoryTest {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getAllCommands()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAllCommands() {
        Assert.assertNotNull(this.commandRepository);
        @Nullable final Collection<AbstractCommand> allCommands = this.commandRepository.getAllCommands();
        Assert.assertNotNull(allCommands);
        Assert.assertNotEquals(0, allCommands.size());
    }

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getAllTerminalCommands()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAllTerminalCommands() {
        Assert.assertNotNull(this.commandRepository);
        @Nullable final Collection<AbstractCommand> allTerminalCommands = this.commandRepository.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
        Assert.assertNotEquals(0, allTerminalCommands.size());
    }

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getAllArgumentCommands()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAllArgumentCommands() {
        Assert.assertNotNull(this.commandRepository);
        @Nullable final Collection<AbstractCommand> allArgumentCommands = this.commandRepository.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
        Assert.assertNotEquals(0, allArgumentCommands.size());
    }

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getTerminalCommand(command)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetTerminalCommand() {
        Assert.assertNotNull(this.commandRepository);
        @NotNull final String command = "help";
        Assert.assertNotNull(command);
        @Nullable final AbstractCommand terminalCommand = this.commandRepository.getTerminalCommand(command);
        Assert.assertNotNull(terminalCommand);
    }

    @Test
    @TestCaseName("Run testNegativeInitialCommands for getArgumentCommand(argument)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetArgumentCommand() {
        Assert.assertNotNull(this.commandRepository);
        @NotNull final String argument = "-h";
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand command = this.commandRepository.getArgumentCommand(argument);
        Assert.assertNotNull(command);
    }

}