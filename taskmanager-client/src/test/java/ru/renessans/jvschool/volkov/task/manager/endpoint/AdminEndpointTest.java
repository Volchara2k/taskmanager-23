package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AdminEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = endpointLocator.getAuthenticationEndpoint();

    @NotNull
    private final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

    @Test
    @Ignore
    @TestCaseName("Run testCloseAllSessions for closeAllSessions(session)")
    public void testCloseAllSessions() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean closeSessions = this.adminEndpoint.closeAllSessions(open);
        Assert.assertTrue(closeSessions);
    }

    @Test
    @TestCaseName("Run testGetAllSessions for getAllSessions(session)")
    public void testGetAllSessions() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<Session> allSessions = this.adminEndpoint.getAllSessions(open);
        Assert.assertNotNull(allSessions);
        Assert.assertNotEquals(0, allSessions.size());
    }

    @Test
    @TestCaseName("Run testSignUpUserWithUserRole for signUpUserWithUserRole(session, \"{0}\", \"{1}\", {2})")
    public void testSignUpUserWithUserRole() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addUser = this.adminEndpoint.signUpUserWithUserRole(
                open, login, password, UserRole.USER
        );
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        Assert.assertEquals(UserRole.USER, addUser.getRole());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

    @Test
    @TestCaseName("Run testDeleteUserById for deleteUserById(session, \"{0}\", \"{1}\")")
    public void testDeleteUserById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User deleteUser = this.adminEndpoint.deleteUserById(open, addRecord.getId());
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
    }

    @Test
    @TestCaseName("Run testDeleteUserByLogin for deleteUserByLogin(session, \"{0}\", \"{1}\")")
    public void testDeleteUserByLogin( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
    }

    @Test
    @Ignore
    @TestCaseName("Run testDeleteAllUsers for deleteAllUsers(session)")
    public void testDeleteAllUsers( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<User> deleteAll = this.adminEndpoint.deleteAllUsers(open);
        Assert.assertNotNull(deleteAll);
        Assert.assertNotEquals(0, deleteAll.size());
    }

    @Test
    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(session, \"{0}\", \"{1}\")")
    public void testLockUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);
        Assert.assertEquals(addRecord.getId(), lockUser.getId());
        Assert.assertEquals(login, lockUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), lockUser.getRole());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(session, \"{0}\", \"{1}\")")
    public void testUnlockUserByLogin( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @Nullable final User lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);

        @Nullable final User unlockUser = this.adminEndpoint.unlockUserByLogin(open, login);
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
        Assert.assertEquals(login, unlockUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), unlockUser.getRole());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

    @Test
    @TestCaseName("Run testGetAllUsers for getAllUsers(session)")
    public void testGetAllUsers() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<User> getUser = this.adminEndpoint.getAllUsers(open);
        Assert.assertNotNull(getUser);
        Assert.assertNotEquals(0, getUser.size());
    }

    @Test
    @TestCaseName("Run testGetAllUsersTasks for getAllUsersTasks(session)")
    public void testGetAllUsersTasks() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<Task> allUsersTasks = this.adminEndpoint.getAllUsersTasks(open);
        Assert.assertNotNull(allUsersTasks);
        Assert.assertNotEquals(0, allUsersTasks.size());
    }

    @Test
    @TestCaseName("Run testGetAllUsersProjects for getAllUsersProjects(session)")
    public void testGetAllUsersProjects( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<Project> allUsersProjects = this.adminEndpoint.getAllUsersProjects(open);
        Assert.assertNotNull(allUsersProjects);
        Assert.assertNotEquals(0, allUsersProjects.size());
    }

    @Test
    @TestCaseName("Run testGetUserById for getUserById(session, \"{0}\", \"{1}\")")
    public void testGetUserById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User getUser = this.adminEndpoint.getUserById(open, addRecord.getId());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

    @Test
    @TestCaseName("Run testGetUserByLogin for getUserByLogin(session, \"{0}\", \"{1}\")")
    public void testGetUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User getUser = this.adminEndpoint.getUserByLogin(open, addRecord.getLogin());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfileById(session, \"{0}\", \"{1}\", \"{2}\")")
    public void testEditProfileById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User editUser = this.adminEndpoint.editProfileById(open, addRecord.getId(), "newFirstName");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(addRecord.getId(), editUser.getId());
        Assert.assertEquals(login, editUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), editUser.getRole());
        Assert.assertEquals("newFirstName", editUser.getFirstName());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

    @Test
    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(session, \"{0}\", \"{1}\", \"{2}\")")
    public void testUpdatePasswordById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User updatePassword = this.adminEndpoint.updatePasswordById(open, addRecord.getId(), "newPassword");
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
        Assert.assertEquals(login, updatePassword.getLogin());
        Assert.assertEquals(addRecord.getRole(), updatePassword.getRole());
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
    }

}