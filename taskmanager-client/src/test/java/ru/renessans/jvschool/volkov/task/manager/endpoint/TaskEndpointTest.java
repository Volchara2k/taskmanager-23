package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class TaskEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();

    @Test
    @TestCaseName("Run testAddTask for addTask(session, {0}, {1})")
    public void testAddTask() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);

        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(
                open, title, description
        );
        Assert.assertNotNull(addTask);
        Assert.assertEquals(title, addTask.getTitle());
        Assert.assertEquals(description, addTask.getDescription());
    }

    @Test
    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex({0}, 0, {1})")
    public void testUpdateTaskByIndex() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final Task updateTask =
                this.taskEndpoint.updateTaskByIndex(open, 0, title, description);
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(title, updateTask.getTitle());
        Assert.assertEquals(description, updateTask.getDescription());
    }

    @Test
    @TestCaseName("Run testUpdateTaskById for updateTaskById({0}, 0, {1})")
    public void testUpdateTaskById( ) {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Task getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final Task updateTask = this.taskEndpoint.updateTaskById(
                open, getTask.getId(), title, description
        );
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(getTask.getId(), updateTask.getId());
        Assert.assertEquals(getTask.getUserId(), updateTask.getUserId());
        Assert.assertEquals(title, updateTask.getTitle());
        Assert.assertEquals(description, updateTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteTaskById for deleteTaskById({0}, 0, {1})")
    public void testDeleteTaskById() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final Task deleteTask = this.taskEndpoint.deleteTaskById(open, addTask.getId());
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(addTask.getId(), deleteTask.getId());
        Assert.assertEquals(addTask.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(addTask.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(addTask.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex({0}, 0, {1})")
    public void testDeleteTaskByIndex() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);
        @NotNull final Task getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);

        @Nullable final Task deleteTask = this.taskEndpoint.deleteTaskByIndex(open, 0);
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(getTask.getId(), deleteTask.getId());
        Assert.assertEquals(getTask.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(getTask.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(getTask.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle({0}, 0, {1})")
    public void testDeleteTaskByTitle() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final Task deleteTask = this.taskEndpoint.deleteTaskByTitle(open, addTask.getTitle());
        Assert.assertNotNull(deleteTask);
        Assert.assertEquals(addTask.getId(), deleteTask.getId());
        Assert.assertEquals(addTask.getUserId(), deleteTask.getUserId());
        Assert.assertEquals(addTask.getTitle(), deleteTask.getTitle());
        Assert.assertEquals(addTask.getDescription(), deleteTask.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks({0}, 0, {1})")
    public void testDeleteAllTasks() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final Collection<Task> deleteTasks = this.taskEndpoint.deleteAllTasks(open);
        Assert.assertNotNull(deleteTasks);
        Assert.assertNotEquals(0, deleteTasks.size());
        final boolean isUserTasks = deleteTasks.stream().allMatch(entity -> open.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetTaskById for getTaskById({0}, 0, {1})")

    public void testGetTaskById() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final Task getTask = this.taskEndpoint.getTaskById(open, addTask.getId());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTask.getId(), getTask.getId());
        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex({0}, 0, {1})")

    public void testGetTaskByIndex() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final Task getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);
        Assert.assertEquals(open.getUserId(), getTask.getUserId());
    }

    @Test
    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle({0}, 0, {1})")

    public void testGetTaskByTitle() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final Task addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final Task getTask = this.taskEndpoint.getTaskByTitle(open, addTask.getTitle());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTask.getId(), getTask.getId());
        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
    }

    @Test
    @TestCaseName("Run testGetAllTasks for getAllTasks({0}, 0, {1})")

    public void testGetAllTasks() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<Task> getAllTasks = this.taskEndpoint.getAllTasks(open);
        Assert.assertNotNull(getAllTasks);
        Assert.assertNotEquals(0, getAllTasks.size());
        final boolean isUserTasks = getAllTasks.stream().allMatch(entity -> open.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

}