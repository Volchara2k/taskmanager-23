package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class UserEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = endpointLocator.getAuthenticationEndpoint();

    @NotNull
    private final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

    @NotNull
    private final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();

    @Test
    @TestCaseName("Run testGetUser for getUser(session)")
    public void testGetUser() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User getUser = this.userEndpoint.getUser(open);
        Assert.assertNotNull(getUser);
        Assert.assertEquals(UserRole.ADMIN, getUser.getRole());
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfile(session, \"{2}\")")
    public void testEditProfile() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User editUser = this.userEndpoint.editProfile(open, "newFirstName");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(DemoDataConst.USER_TEST_LOGIN, editUser.getLogin());
        Assert.assertEquals(UserRole.USER, editUser.getRole());
        Assert.assertEquals("newFirstName", editUser.getFirstName());
    }

    @Test
    @TestCaseName("Run testEditProfileWithLastName for editProfileWithLastName(session, \"{2}\", \"{2}\")")
    public void testEditProfileWithLastName( ) {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User editUser = this.userEndpoint.editProfileWithLastName(open, "newData", "newData");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(DemoDataConst.USER_DEFAULT_LOGIN, editUser.getLogin());
        Assert.assertEquals(UserRole.USER, editUser.getRole());
        Assert.assertEquals("newData", editUser.getFirstName());
        Assert.assertEquals("newData", editUser.getLastName());
    }

    @Test
    @TestCaseName("Run testUpdatePassword for updatePassword(session, \"{2}\")")
    public void testUpdatePassword( ) {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final User addUser = this.authEndpoint.signUpUser(login, password);
        Assert.assertNotNull(addUser);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                login, password);
        Assert.assertNotNull(open);

        @Nullable final User updatePassword = this.userEndpoint.updatePassword(open, "newPassword");
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(login, updatePassword.getLogin());

        @NotNull final Session openAdminSession = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openAdminSession);
        @NotNull final User deleteUser = this.adminEndpoint.deleteUserByLogin(openAdminSession, login);
        Assert.assertNotNull(deleteUser);
    }

}